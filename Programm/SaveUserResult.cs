﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace Programm
{
    public static class SaveUserResult
    {
        //получаем из имени вершины тестов название темы(занятия)
        public static string ParseTestNodeName(string vertex_name)
        {
            string[] split = vertex_name.Split('.');
            return (split[1] + "." + split[2]);
        }

        //запись результата теста в БД
        //ВАЖНО: есть возможность пересдачи результата
        public static void FillTestResult(string work_name, string test_result, int curUserId, string mode)
        {
            //string [] split = work_name.Split(' ');
            //if (split[0] == " ")
            //{
            //    work_name = "";
            //    for (int i = 0; i < split.Length; i++)
            //        work_name = work_name + split[i];
            // }
            int idToFill = IsRowExists(work_name, curUserId); //здесь нужный id для записи или -1
            //если строчка уже есть
            if (idToFill != -1)
            {
                if (mode == "тест") FillTestResult(test_result, idToFill);
                else if (mode == "лаб") FillLabResult(test_result, idToFill);
                else if (mode == "срс") FillSrsResult(test_result, idToFill);


            }
            //если строчки нет, то нужно ее создать и вписать результат
            else
            {
                idToFill = CreateUserResultRow(curUserId, work_name);

                if (mode == "тест") FillTestResult(test_result, idToFill);
                else if (mode == "лаб") FillLabResult(test_result, idToFill);
                else if (mode == "срс") FillSrsResult(test_result, idToFill);
            }
        }

        //заполняем ячейку с результатом теста
        public static void FillTestResult(string test_result, int idToFill)
        {
            using (var dataBase = new MyDatabase())
            {
                string QueryFillResult = @"UPDATE Res_Work SET Res_Test=:Res_Test where id=:id";
                SQLiteCommand commandQueryFillResult = new SQLiteCommand(QueryFillResult, dataBase.sConnect);
                commandQueryFillResult.Parameters.AddWithValue("Res_Test", test_result);
                commandQueryFillResult.Parameters.AddWithValue("id", idToFill);
                commandQueryFillResult.ExecuteNonQuery();
            }
        }

        public static void FillLabResult(string test_result, int idToFill)
        {
            using (var dataBase = new MyDatabase())
            {
                string QueryFillResult = @"UPDATE Res_Work SET Res_Lab=:Res_Lab where id=:id";
                SQLiteCommand commandQueryFillResult = new SQLiteCommand(QueryFillResult, dataBase.sConnect);
                commandQueryFillResult.Parameters.AddWithValue("Res_Lab", test_result);
                commandQueryFillResult.Parameters.AddWithValue("id", idToFill);
                commandQueryFillResult.ExecuteNonQuery();
            }
        }

        public static void FillSrsResult(string test_result, int idToFill)
        {
            using (var dataBase = new MyDatabase())
            {
                string QueryFillResult = @"UPDATE Res_Work SET Res_Srs=:Res_Srs where id=:id";
                SQLiteCommand commandQueryFillResult = new SQLiteCommand(QueryFillResult, dataBase.sConnect);
                commandQueryFillResult.Parameters.AddWithValue("Res_Srs", test_result);
                commandQueryFillResult.Parameters.AddWithValue("id", idToFill);
                commandQueryFillResult.ExecuteNonQuery();
            }
        }

        //создаем строчку для записи результата студента. Возвращает строчку в которую записать
        public static int CreateUserResultRow(int curUserId, string name_Work)
        {
            int lastId; //последний id записей
            using (var dataBase = new MyDatabase())
            {
                //находим id для записи
                string QueryGetLastId = @" SELECT max(id) FROM Res_Work";
                SQLiteCommand commandQueryGetLastId = new SQLiteCommand(QueryGetLastId, dataBase.sConnect);
                if (commandQueryGetLastId.ExecuteScalar() == DBNull.Value) //если таблица совсем пустая
                    lastId = 0;
                else
                    lastId = Convert.ToInt32(commandQueryGetLastId.ExecuteScalar());

                //создаем строчку
                string QueryCreateRow = @"INSERT INTO Res_Work (id,id_Aut,Name_Work) VALUES (@id,@id_Aut,@Name_Work)";
                SQLiteCommand commandQueryCreateRow = new SQLiteCommand(QueryCreateRow, dataBase.sConnect);
                commandQueryCreateRow.Parameters.AddWithValue("id", lastId + 1);
                commandQueryCreateRow.Parameters.AddWithValue("id_Aut", curUserId);
                commandQueryCreateRow.Parameters.AddWithValue("Name_Work", name_Work);
                commandQueryCreateRow.ExecuteNonQuery();
            }
            return (lastId + 1);
        }

        //проверяем есть ли в таблице строка с таким user_id+name_work. Если есть - возвращаем ее номер, если нет, то -1
        public static int IsRowExists(string work_name, int curUserId)
        {
            using (var dataBase = new MyDatabase())
            {
                string QueryIsRowExists = @"SELECT id FROM Res_Work WHERE id_Aut=@id_Aut AND Name_Work=@Name_Work";
                SQLiteCommand commandQueryIsRowExists = new SQLiteCommand(QueryIsRowExists, dataBase.sConnect);
                commandQueryIsRowExists.Parameters.Add(new SQLiteParameter("id_Aut", curUserId));
                commandQueryIsRowExists.Parameters.Add(new SQLiteParameter("Name_Work", work_name));
                string result = Convert.ToString(commandQueryIsRowExists.ExecuteScalar());
                if (result == "") return -1;
                else return Convert.ToInt32(result);

            }
        }
    }
}
