﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Programm
{
    public partial class Hello : Form
    {
        public Hello()
        {
            InitializeComponent();
        }

        private void btnAuth_Click(object sender, EventArgs e)
        {
            Visible = false;
            new Authentication().ShowDialog();
            Visible = true;
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            Visible = false;
            new Registration().ShowDialog();
            Visible = true;
        }
    }
}
