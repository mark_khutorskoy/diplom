﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Programm
{
    public partial class SelectGroupForResultDGV : Form
    {
        public SelectGroupForResultDGV()
        {
            InitializeComponent();

            GetGroupList();
        }

        //заполенение комбобокса
        public void GetGroupList()
        {
            using (var dataBase = new MyDatabase())
            {
                string queryGetGroups = @"SELECT DISTINCT [Group] from Authentication";
                SQLiteCommand commandqueryGetGroups = new SQLiteCommand(queryGetGroups, dataBase.sConnect);
                SQLiteDataReader reader = commandqueryGetGroups.ExecuteReader();
                while (reader.Read())
                {
                    if((string)reader["Group"]!="admin")
                    cbGroups.Items.Add(reader["Group"]);
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (cbGroups.SelectedIndex != -1)
            {
                Visible = false;
                new _TH_Results((cbGroups.SelectedItem).ToString()).ShowDialog();
                Close();
            }
            else MessageBox.Show("Выберете группу");
        }

        private void SelectGroupForResultDGV_Load(object sender, EventArgs e)
        {
            
        }
    }
}
