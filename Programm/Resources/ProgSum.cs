using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace ������
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] outline = new string[100];//������ ����� ��� ������ �������� ������
            int j = 0; // �������
            int Sum = 0; //���������� ��� ������������ ���������

            using (StreamReader sr = new StreamReader("INPUT.txt"))
            {
                string line;
                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine();//���������� ����������
                    //��������� ���������, ����������� �������� � ��������� � ������ items
                    int[] items = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)).ToArray();

                    Sum = 0;
                    for (int i = 0; i < items.Length; i++)
                    {
                        //   �������� � ���������� ������
                        Sum += items[i];
                    }

                    //��������� ����������� � ������ ��� ������
                    outline[j] += Sum.ToString();
                    j++;
                }
            }

            using (StreamWriter writer = new StreamWriter("OUTPUT.txt")) //������ � ���� output
            {
                writer.Write(string.Join("\n", outline));// ����� ������
            }
        }

    }
}
