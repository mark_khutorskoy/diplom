﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Programm
{
    public partial class MainWindow : Form
    {
        bool isTeacher;
        string FIOstring;
        string Group_string;
        int curUserId;

        public MainWindow()
        {
            InitializeComponent();
            int id = Properties.Settings.Default.UserId;
            bool _isTeacher = Properties.Settings.Default.IsTeacher;


            curUserId = id; //запомнили id текущего юзера. Нужно будет в форме TV

            GetInfo(id, ref FIOstring, ref Group_string); //Инфо о юзере в лейблы
            
             //если преподаватель
            if (_isTeacher)
            {
                StudentMode.Parent = null;
                TeacherMode.Parent = tcMode;
                lblFIOTH.Text = FIOstring; 
            }
            else
            {
                TeacherMode.Parent = null;
                StudentMode.Parent = tcMode;
                lblFIO.Text = FIOstring;
                lblGroup.Text = Group_string;
            }
             //передатчик в другую форму
            isTeacher = _isTeacher;

        }  

        public void GetInfo(int id, ref string FIOstring, ref string group)
        {
            using (var dataBase = new MyDatabase())
            {
                string queryGetLastName = @"select Last_Name from Authentication where id = @id";
                SQLiteCommand commandqueryGetLastName = new SQLiteCommand(queryGetLastName, dataBase.sConnect);
                commandqueryGetLastName.Parameters.Add(new SQLiteParameter("@id", id));
                FIOstring = Convert.ToString(commandqueryGetLastName.ExecuteScalar());

                if (FIOstring != "Admin")
                {

                    string queryGetFirstName = @"select First_Name from Authentication where id = @id";
                    SQLiteCommand commandqueryGetFirstName = new SQLiteCommand(queryGetFirstName, dataBase.sConnect);
                    commandqueryGetFirstName.Parameters.Add(new SQLiteParameter("@id", id));
                    FIOstring = FIOstring + ' ' + Convert.ToString(commandqueryGetFirstName.ExecuteScalar());

                    string queryGetPatronymicName = @"select Patronymic_Name from Authentication where id = @id";
                    SQLiteCommand commandqueryGetPatronymicName = new SQLiteCommand(queryGetPatronymicName, dataBase.sConnect);
                    commandqueryGetPatronymicName.Parameters.Add(new SQLiteParameter("@id", id));
                    FIOstring = FIOstring + ' ' + Convert.ToString(commandqueryGetPatronymicName.ExecuteScalar());

                    string queryGetGroup = "select \"Group\" from Authentication where id = @id";
                    SQLiteCommand commandqueryGetGroup = new SQLiteCommand(queryGetGroup, dataBase.sConnect);
                    commandqueryGetGroup.Parameters.Add(new SQLiteParameter("@id", id));
                    group = Convert.ToString(commandqueryGetGroup.ExecuteScalar());
                }
            }
        }

        private void btnOpenTV_Click(object sender, EventArgs e)
        {
            Visible = false;
            new TV(lblFIO.Text, lblGroup.Text, isTeacher, curUserId).ShowDialog();
            Visible = true;
        }

        private void btnDZLoad_Click(object sender, EventArgs e)
        {
            Visible = false;
            new TestTasks(false, true, curUserId).ShowDialog();
            Visible = true;
        }

        private void btnOpenResults_Click(object sender, EventArgs e) //запуск dgv
        {
            Visible = false;
            new ResultStudent(curUserId).ShowDialog();
            Visible = true;
        }


        private void btnTHOpenTV_Click(object sender, EventArgs e)
        {
            Visible = false;
            new TV(lblFIOTH.Text, "", isTeacher, curUserId).ShowDialog();
            Visible = true;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Visible = false;
            new _TH_DataBaseRevise().ShowDialog();
            Visible = true;
        }

        private void btnStuResults_Click(object sender, EventArgs e)
        {
            Visible = false;
            new SelectGroupForResultDGV().ShowDialog();
            Visible = true;
        }
    }
}
