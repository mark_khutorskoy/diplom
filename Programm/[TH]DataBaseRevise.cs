﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Programm
{
    public partial class _TH_DataBaseRevise : Form
    {
        //выходные и выходные
        List<String> input;
        List<String> output;
        int examination_iterator; //номер записи в examination

        List<int> idLabSRSToRemove; //здесь хранятся зависимые id_Lab и id_SRS для удаления var
        public _TH_DataBaseRevise()
        {
            InitializeComponent();

            TestTasks _testtasks = new TestTasks(true, true,0);
            _testtasks.SetHomeWorkLab(cbWork);
            _testtasks.SetHomeWorkLab(cbWorkEdit);
            _testtasks.SetHomeWorkLab(cbWorkDelete);
        }

        //переимновка!
        private void cbLab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbVariant.SelectedIndex != -1) FillText(cbWork.Text, cbVariant.Text, tbTaskText);

            if (cbWork.SelectedIndex != -1)
            {
                string Name_Lab = cbWork.SelectedItem.ToString();
                using (var dataBase = new MyDatabase())
                {
                    string queryGetVarList = @"select DISTINCT Work.Var from Work where Work.Name_Work = @Name_Lab ";
                    SQLiteCommand commandGetVarList = new SQLiteCommand(queryGetVarList, dataBase.sConnect);
                    commandGetVarList.Parameters.Add(new SQLiteParameter("@Name_Lab", Name_Lab));
                    var Reader = commandGetVarList.ExecuteReader();
                    cbVariant.Items.Clear();
                    while (Reader.Read())
                    {
                        cbVariant.Items.Add(Reader["var"]);
                    }
                }
            }
        }

        //кнопка добавить
        private void btnOK_Click(object sender, EventArgs e)
        {
            using (var dataBase = new MyDatabase())
            {
                if(cbWork.Text=="" ||cbVariant.Text==""||cbType.Text==""||tbTaskText.Text=="")
                {
                    MessageBox.Show("Заполните поля: раздел, тема, тип работы");
                    return;
                }
                //нашли макс. id_лабы
                string CommandText0 = @" SELECT max(id_Lab) FROM Work"; 
                SQLiteCommand commandGroup0 = new SQLiteCommand(CommandText0, dataBase.sConnect);
                int maxIdLab;
                //проверка на пустую БД
                if (commandGroup0.ExecuteScalar()!= DBNull.Value) maxIdLab = Convert.ToInt32(commandGroup0.ExecuteScalar()); //запомнили макс. id лабы
                else  maxIdLab = 0;

                //нашли макс. id_срс
                string CommandText1 = @" SELECT max(id_Srs) FROM Work "; 
                SQLiteCommand commandGroup1 = new SQLiteCommand(CommandText1, dataBase.sConnect);
                int maxIdSRS;
                if (commandGroup1.ExecuteScalar() != DBNull.Value) maxIdSRS = Convert.ToInt32(commandGroup1.ExecuteScalar()); //запомнили макс. id срс
                else maxIdSRS = 0;

                int maxId; //запомнили в итоге максимальный из двух столбцов
                if (maxIdLab > maxIdSRS) maxId = maxIdLab;
                else maxId = maxIdSRS;

                MessageBox.Show("Максимальный id у двух колонок " + Convert.ToString(maxId));

                string work = cbType.Text; //поняли тип работы
                //bool IsEmpty = false; //тут будет хранится нашлось ли свободное место. Null, если да
                int idToFill; //номер строки (простой id) в Work куда будем записывать лабу/срс
                switch (work)
                {
                    // case "Лабораторная": //!!!
                    case "Аудиторная":
                        //ищем свободное место для записи лабы, если есть
                        string QueryFindEmptyPlaceForLab = @"SELECT id FROM Work WHERE Name_Work=:Name_Work AND Var=:Var AND id_Lab is null";
                        SQLiteCommand commandQueryFindEmptyPlaceForLab = new SQLiteCommand(QueryFindEmptyPlaceForLab, dataBase.sConnect);
                        commandQueryFindEmptyPlaceForLab.Parameters.Add(new SQLiteParameter("Name_work", cbWork.Text));
                        commandQueryFindEmptyPlaceForLab.Parameters.Add(new SQLiteParameter("Var", cbVariant.Text));
                       // IsEmpty = commandQueryFindEmptyPlaceForLab.ExecuteScalar();
                        MessageBox.Show(Convert.ToString(commandQueryFindEmptyPlaceForLab.ExecuteScalar()));
                        if (commandQueryFindEmptyPlaceForLab.ExecuteScalar() != null) //если нашлось
                        {
                            idToFill = Convert.ToInt32(commandQueryFindEmptyPlaceForLab.ExecuteScalar());
                            AddLab(maxId, idToFill);                     
                        }
                        else
                        {
                            AddWork(); //добавили новый work 

                            //Name_work+Var дает однозначно идентифицировать id
                            //Ищем последнюю запись такого типа, т.к. она может и не быть последней записью в БД(напр. добавление к старой)
                            string QueryFindLastThatTypeRecord = @" SELECT max(id) FROM Work where Name_work=:Name_Work and Var=:Var "; //ищем последнюю подходящую запись (будет только что созданная)
                            SQLiteCommand commandQueryFindLastThatTypeRecord = new SQLiteCommand(QueryFindLastThatTypeRecord, dataBase.sConnect);
                            commandQueryFindLastThatTypeRecord.Parameters.Add(new SQLiteParameter("Name_work", cbWork.Text));
                            commandQueryFindLastThatTypeRecord.Parameters.Add(new SQLiteParameter("Var", cbVariant.Text));
                            idToFill = Convert.ToInt32(commandQueryFindLastThatTypeRecord.ExecuteScalar()); //запоминаем максимальный id Work'a, сюда будем записывать
                            MessageBox.Show("будем писать в запись с id " + Convert.ToString(idToFill));
                            AddLab(maxId, idToFill);
                        }

                        break;

                    case "СРС":
                        //ищем свободное место для записи лабы, если есть
                        string QueryFindEmptyPlaceForSRS = @"SELECT id FROM Work WHERE Name_Work=:Name_Work AND Var=:Var AND id_Srs is null";
                        SQLiteCommand commandQueryFindEmptyPlaceForSRS = new SQLiteCommand(QueryFindEmptyPlaceForSRS, dataBase.sConnect);
                        commandQueryFindEmptyPlaceForSRS.Parameters.Add(new SQLiteParameter("Name_work", cbWork.Text));
                        commandQueryFindEmptyPlaceForSRS.Parameters.Add(new SQLiteParameter("Var", cbVariant.Text));
                        //IsEmpty = commandQueryFindEmptyPlaceForSRS.ExecuteScalar();

                        if (commandQueryFindEmptyPlaceForSRS.ExecuteScalar() != null) //если нашлось
                        {
                            
                            idToFill = Convert.ToInt32(commandQueryFindEmptyPlaceForSRS.ExecuteScalar());
                            AddSRS(maxId, idToFill);
                        }
                        else
                        {
                            AddWork(); //добавили новый work 

                            //Name_work+Var дает однозначно идентифицировать id
                            //Ищем последнюю запись такого типа, т.к. она может и не быть последней записью в БД(напр. добавление к старой)
                            string QueryFindLastThatTypeRecord = @" SELECT max(id) FROM Work where Name_work=:Name_Work and Var=:Var "; //ищем последнюю подходящую запись (будет только что созданная)
                            SQLiteCommand commandQueryFindLastThatTypeRecord = new SQLiteCommand(QueryFindLastThatTypeRecord, dataBase.sConnect);
                            commandQueryFindLastThatTypeRecord.Parameters.Add(new SQLiteParameter("Name_work", cbWork.Text));
                            commandQueryFindLastThatTypeRecord.Parameters.Add(new SQLiteParameter("Var", cbVariant.Text));
                            idToFill = Convert.ToInt32(commandQueryFindLastThatTypeRecord.ExecuteScalar()); //запоминаем максимальный id Work'a, сюда будем записывать
                            MessageBox.Show("будем писать в запись с id " + Convert.ToString(idToFill));
                            AddSRS(maxId, idToFill);
                        }
                        break;
                }

                //делаем соотвествующую запись в таблице Text_work
                string CommandText4 = @"INSERT INTO Text_Work (id,Text) VALUES (@id, @Text)";
                SQLiteCommand commandGroup4 = new SQLiteCommand(CommandText4, dataBase.sConnect);
                commandGroup4.Parameters.Add(new SQLiteParameter("id", maxId+1));
                commandGroup4.Parameters.Add(new SQLiteParameter("Text", tbTaskText.Text));
                commandGroup4.ExecuteNonQuery();
                MessageBox.Show("Запись в Text_Work сделана");

                //делаем соотвествующую запись в таблице Examination, если что-то введено в текстбоксы
                if (tbInputData.Text != "" && tbOutputData.Text != "")
                {
                    string CommandText5 = @"INSERT INTO Examination (id_Text_Work, Input_Data, Output_Data) VALUES (@id_Text_Work, @Input_Data, @Output_Data)";
                    SQLiteCommand commandGroup5 = new SQLiteCommand(CommandText5, dataBase.sConnect);
                    commandGroup5.Parameters.Add(new SQLiteParameter("id_Text_Work", maxId+1));
                    commandGroup5.Parameters.Add(new SQLiteParameter("Input_Data",tbInputData.Text ));
                    commandGroup5.Parameters.Add(new SQLiteParameter("Output_Data",tbOutputData.Text));
                    commandGroup5.ExecuteNonQuery();
                    MessageBox.Show("Запись в Examination сделана");
                }
            }

            //обновили доступные данные
            TestTasks _testtasks = new TestTasks(true, true,0);
            _testtasks.SetHomeWorkLab(cbWork);
            _testtasks.SetHomeWorkLab(cbWorkEdit);
            _testtasks.SetHomeWorkLab(cbWorkDelete);
        }

        //добавление темы и "варианта"
        public void AddWork()
        {
            if (cbVariant.Text == "" || cbWork.Text == "") MessageBox.Show("Напишите тему и вариант");
            else
            {
                using (var dataBase = new MyDatabase())
                {
                    string CommandText = @" SELECT max(id) FROM Work";
                    SQLiteCommand commandGroup = new SQLiteCommand(CommandText, dataBase.sConnect);
                    var max_id = commandGroup.ExecuteScalar(); //запоминаем максимальный id Work'a

                    // MessageBox.Show("Max ID: " + max_id);
                    //добавляем новый work и var
                    string CommandText2 = @"INSERT INTO Work (id,Name_Work, Var) VALUES  (@id,@Name_Work,@Var)";
                    SQLiteCommand commandGroup2 = new SQLiteCommand(CommandText2, dataBase.sConnect);
                    commandGroup2.Parameters.Add(new SQLiteParameter("id", ((max_id == DBNull.Value) ? 1 : Convert.ToInt32(max_id) + 1))); //вставляем след. id, если вернулся NULL, то записей нет.
                    commandGroup2.Parameters.Add(new SQLiteParameter("Name_Work", cbWork.Text));
                    commandGroup2.Parameters.Add(new SQLiteParameter("Var", cbVariant.Text));
                    commandGroup2.ExecuteNonQuery();
                    MessageBox.Show("Добавлен work");
                }
            }


        }
        public void AddLab(int id_Lab, int id)
        {
            using (var dataBase = new MyDatabase())
            {
                string QueryWriteNewLab = @"UPDATE Work SET id_Lab = :id_Lab where id=:id";
                SQLiteCommand commandQueryWriteNewLab = new SQLiteCommand(QueryWriteNewLab, dataBase.sConnect);
                commandQueryWriteNewLab.Parameters.Add(new SQLiteParameter("id_Lab", id_Lab + 1));
                commandQueryWriteNewLab.Parameters.Add(new SQLiteParameter("id", id));
                commandQueryWriteNewLab.ExecuteNonQuery();            
            }       
        }

        public void AddSRS(int id_SRS, int id)
        {
            using (var dataBase = new MyDatabase())
            {
                string QueryWriteNewSRS = @"UPDATE Work SET id_Srs = :id_Srs where id=:id";
                SQLiteCommand commandQueryWriteNewSRS = new SQLiteCommand(QueryWriteNewSRS, dataBase.sConnect);
                commandQueryWriteNewSRS.Parameters.Add(new SQLiteParameter("id_Srs", id_SRS + 1));
                commandQueryWriteNewSRS.Parameters.Add(new SQLiteParameter("id", id));
                commandQueryWriteNewSRS.ExecuteNonQuery();
            }
        
        }

        //код авто-заполнения поля "текст"
        public void FillText(string section, string topic, TextBox x)
        {
            //избавляемся от ; в теме
            string[] split = topic.Split(';');
            topic = split[0] + ". " + split[1];

            string res = section + ". " + topic;
            x.Text = res;
        }

        //отображение выпадающего списка 2-го бокса
        //триггер смены 1-го бокса
        private void cbWorkEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbWorkEdit.SelectedIndex != -1)
            {
                tbTextEdit.Text = "";
                cbIdLabSRS.Items.Clear();//почистили нижний комбобокс

                string Name_Lab = cbWorkEdit.SelectedItem.ToString();
                using (var dataBase = new MyDatabase())
                {
                    string queryCount = @"select DISTINCT Work.Var from Work where Work.Name_Work = @Name_Lab ";
                    SQLiteCommand commandVar = new SQLiteCommand(queryCount, dataBase.sConnect);
                    commandVar.Parameters.Add(new SQLiteParameter("@Name_Lab", Name_Lab));
                    var Reader = commandVar.ExecuteReader();
                    cbVarEdit.Items.Clear();
                    while (Reader.Read())
                    {
                        cbVarEdit.Items.Add(Reader["var"]);
                    }

                }
            }
        }

        //отображение выпадающего списка 4-го бокса
        //триггер смены типа работы
        private void cbTypeEdit_SelectedIndexChanged(object sender, EventArgs e)
        {      
            if (cbTypeEdit.SelectedIndex != -1 || cbVarEdit.SelectedIndex!=-1 || cbWorkEdit.SelectedIndex!=-1) //если выбран тип работы
            {
                tbTextEdit.Text = "";
                string Name_Work = cbWorkEdit.SelectedItem.ToString(); //запомнили work
                string Var_Work = cbVarEdit.SelectedItem.ToString(); //запомнили var
                using (var dataBase = new MyDatabase())
                {
                    //if (cbTypeEdit.Text == "Лабораторная") //!!!
                    if (cbTypeEdit.Text == "Аудиторная")       
                    {
                        string queryCount = @"select Work.id_Lab from Work where Work.Name_Work = @Name_Work and Work.Var = @Var and Work.id_Lab is not null";
                        SQLiteCommand commandVar = new SQLiteCommand(queryCount, dataBase.sConnect);
                        commandVar.Parameters.Add(new SQLiteParameter("@Name_Work", Name_Work));
                        commandVar.Parameters.Add(new SQLiteParameter("@Var", Var_Work));
                        var Reader = commandVar.ExecuteReader();

                        cbIdLabSRS.Items.Clear();
                        while (Reader.Read())
                        {
                            cbIdLabSRS.Items.Add(Reader["id_Lab"]);
                        }
                    }
                    else //значит выбрана СРС
                    {
                        string queryCount = @"select Work.id_Srs from Work where Work.Name_Work = @Name_Work and Work.Var = @Var and Work.id_Srs is not null";
                        SQLiteCommand commandVar = new SQLiteCommand(queryCount, dataBase.sConnect);
                        commandVar.Parameters.Add(new SQLiteParameter("@Name_Work", Name_Work));
                        commandVar.Parameters.Add(new SQLiteParameter("@Var", Var_Work));
                        var Reader = commandVar.ExecuteReader();

                        cbIdLabSRS.Items.Clear();
                        while (Reader.Read())
                        {
                            cbIdLabSRS.Items.Add(Reader["id_Srs"]);
                        }
                    }
                }
            }
        }

        //отображение текста и входных/выходных
        private void cbIdLabSRS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbIdLabSRS.SelectedIndex != -1) //если выбран тип работы
            {
                tbTextEdit.Text = "";
                using (var dataBase = new MyDatabase())
                {       //отображение текста           
                        string queryCount = @"select Text_Work.Text from Text_Work where Text_Work.id = @id";
                        SQLiteCommand commandVar = new SQLiteCommand(queryCount, dataBase.sConnect);
                        commandVar.Parameters.Add(new SQLiteParameter("@id", Convert.ToInt32(cbIdLabSRS.Text)));
                        tbTextEdit.Text = (commandVar.ExecuteScalar()).ToString();

                        //отображение входных данных и заполнение list'a input
                        input = new List<string>();
                        string QueryInput = @"SELECT Input_Data FROM Examination where id_Text_Work=:id_Text_Work ";
                        SQLiteCommand commandQueryInput = new SQLiteCommand(QueryInput, dataBase.sConnect);
                        commandQueryInput.Parameters.Add(new SQLiteParameter("id_Text_Work", Convert.ToInt32(cbIdLabSRS.Text)));

                        var reader = commandQueryInput.ExecuteReader();
                        while (reader.Read())
                        {
                            input.Add((string)reader["Input_Data"]);
                        }
                        if (input.Count >0)// может быть пустой запрос, если на форме добавления не внесли вх/вых данные
                        {
                            tbInputDataEdit.Text = input[0]; //заполнили первым значением
                            examination_iterator = 0;
                        }
                        else tbInputDataEdit.Text = "Нет данных";

                        //отображение выходных данных и заполнение list'a output
                        output = new List<string>();
                        string QueryOutput = @"SELECT Output_Data FROM Examination where id_Text_Work=:id_Text_Work ";
                        SQLiteCommand commandQueryOutput = new SQLiteCommand(QueryOutput, dataBase.sConnect);
                        commandQueryOutput.Parameters.Add(new SQLiteParameter("id_Text_Work", Convert.ToInt32(cbIdLabSRS.Text)));

                        var readerOutput = commandQueryOutput.ExecuteReader();
                        while (readerOutput.Read())
                        {
                            output.Add((string)readerOutput["Output_Data"]);         
                        }
                        if (output.Count >0)
                        {
                            tbOutputDataEdit.Text = output[0];//заполнили первым значением
                            
                        }
                        else tbOutputDataEdit.Text = "Нет данных";
                }
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            ScrollToRight(tbInputDataEdit, tbOutputDataEdit);
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {        
            ScrollToLeft(tbInputDataEdit, tbOutputDataEdit);
        }

        //кнопка добавить новые вх/вых
        private void pbAddTestData_Click(object sender, EventArgs e)
        {
          if (tbInputDataEdit.Text != "" && tbOutputDataEdit.Text != "" && tbInputData.Text != "Нет данных" && tbOutputData.Text != "Нет данных")
          {

              if (CheckUniqueness() == false) return; //проверка на не повтор вх/вых данных (уникальность)
                  
                      using (var dataBase = new MyDatabase())
                      {
                          string QueryInsertNewExaminationData = @"INSERT INTO Examination (id_Text_Work,Input_Data, Output_Data) VALUES (@id_Text_Work,@Input_Data, @Output_Data)";
                          SQLiteCommand commandQueryInsertNewExaminationData = new SQLiteCommand(QueryInsertNewExaminationData, dataBase.sConnect);
                          commandQueryInsertNewExaminationData.Parameters.Add(new SQLiteParameter("@id_Text_Work", Convert.ToInt32(cbIdLabSRS.Text)));
                          commandQueryInsertNewExaminationData.Parameters.Add(new SQLiteParameter("@Input_Data", tbInputDataEdit.Text));
                          commandQueryInsertNewExaminationData.Parameters.Add(new SQLiteParameter("@Output_Data", tbOutputDataEdit.Text));
                          commandQueryInsertNewExaminationData.ExecuteNonQuery();

                          //обновили листы (как минимум для отображения)
                          input.Add(tbInputDataEdit.Text);
                          output.Add(tbOutputDataEdit.Text);

                          MessageBox.Show("Добавлена запись в Examination");
                      }
          }       
        }

        //кнопка редактирования вх/вых
        private void pbEditTestData_Click(object sender, EventArgs e)
        {
            using (var dataBase = new MyDatabase())
            {
                if (tbInputDataEdit.Text != "" && tbOutputDataEdit.Text != "" && tbInputData.Text != "Нет данных" && tbOutputData.Text != "Нет данных")
                {
                    if (CheckUniqueness() == false) return; //проверка на не повтор вх/вых данных (уникальность)
                   
                    //найдем id записи в Examination
                    string QueryFindExaminationId = @"SELECT id FROM Examination WHERE Input_Data=:Input_Data AND Output_Data=:Output_Data 
                                                                                                               AND id_Text_Work=:id_Text_Work";
                    SQLiteCommand commandQueryFindExaminationId = new SQLiteCommand(QueryFindExaminationId, dataBase.sConnect);
                    commandQueryFindExaminationId.Parameters.Add(new SQLiteParameter("Input_Data", Convert.ToInt32(input[examination_iterator])));
                    commandQueryFindExaminationId.Parameters.Add(new SQLiteParameter("Output_Data",Convert.ToInt32(output[examination_iterator])));
                    commandQueryFindExaminationId.Parameters.Add(new SQLiteParameter("id_Text_Work", Convert.ToInt32(cbIdLabSRS.Text)));
                    int ExaminationId =  Convert.ToInt32(commandQueryFindExaminationId.ExecuteScalar());
                    MessageBox.Show("Изменили запись с id "+ ExaminationId.ToString());


                    //изменили значение входных и выходных
                    string QueryExaminationInsert = @"UPDATE Examination SET Input_Data=:Input_Data, Output_Data =:Output_Data where id=:id";
                    SQLiteCommand commandQueryExaminationInsert = new SQLiteCommand(QueryExaminationInsert, dataBase.sConnect);
                    commandQueryExaminationInsert.Parameters.Add(new SQLiteParameter("id", ExaminationId));
                    commandQueryExaminationInsert.Parameters.Add(new SQLiteParameter("Input_Data", tbInputDataEdit.Text));
                    commandQueryExaminationInsert.Parameters.Add(new SQLiteParameter("Output_Data", tbOutputDataEdit.Text));

                    //изменили данные в листе
                    input[examination_iterator] = tbInputDataEdit.Text;
                    output[examination_iterator] = tbOutputDataEdit.Text;

                    commandQueryExaminationInsert.ExecuteNonQuery();
                    
                    MessageBox.Show("Запись в Examination отредактирована");
                }
            }
        }

        private bool CheckUniqueness()
        {
            bool IsUniq = true;
            for (int find_input = 0; find_input < input.Count; find_input++)
                if (input[find_input] == tbInputDataEdit.Text)
                {
                    for (int find_output = 0; find_output < output.Count; find_output++)
                        if (output[find_input] == tbOutputDataEdit.Text) //значит нашли повтор
                        {
                            IsUniq = false;
                        }
                }
            if (!IsUniq) MessageBox.Show("Такие тестовые данные у данной работы уже есть");
            return IsUniq;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (tbTextEdit.Text != "" && cbIdLabSRS.Text != "")
            {
                using (var dataBase = new MyDatabase())
                {
                    string QueryUpdateText = @"UPDATE Text_Work SET Text=:Text WHERE id=:id";
                    SQLiteCommand commandQueryUpdateText = new SQLiteCommand(QueryUpdateText, dataBase.sConnect);
                    commandQueryUpdateText.Parameters.Add(new SQLiteParameter("Text", tbTextEdit.Text));
                    commandQueryUpdateText.Parameters.Add(new SQLiteParameter("id", cbIdLabSRS.Text)); ;
                    commandQueryUpdateText.ExecuteNonQuery();
                    MessageBox.Show("Текст в Text_Work изменен");
                }
            }
            else if (tbTextEdit.Text == "") MessageBox.Show("Введите текст");
            else MessageBox.Show("Выберите работу в верхней панели");

        }

        private void cbWorkDelete_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbWorkDelete.SelectedIndex != -1)
            {
                tbTextDelete.Text = "";
                cbIdLabSRSDelete.Items.Clear();//почистили нижний комбобокс

                string Name_Lab = cbWorkDelete.SelectedItem.ToString();
                using (var dataBase = new MyDatabase())
                {
                    string queryVars = @"select DISTINCT Work.Var from Work where Work.Name_Work = @Name_Lab ";
                    SQLiteCommand commandqueryVars = new SQLiteCommand(queryVars, dataBase.sConnect);
                    commandqueryVars.Parameters.Add(new SQLiteParameter("@Name_Lab", Name_Lab));
                    var Reader = commandqueryVars.ExecuteReader();
                    cbVarDelete.Items.Clear();
                    while (Reader.Read())
                    {
                        cbVarDelete.Items.Add(Reader["var"]);
                    }

                }
            }
        }

        private void cbVarDelete_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTypeDelete.SelectedIndex != -1 && cbVarDelete.SelectedIndex != -1 && cbWorkDelete.SelectedIndex != -1) //если выбран тип работы
            {
                tbTextDelete.Text = "";
                string Name_Work = cbWorkDelete.SelectedItem.ToString(); //запомнили work
                string Var_Work = cbVarDelete.SelectedItem.ToString(); //запомнили var
                using (var dataBase = new MyDatabase())
                {
                    //if (cbTypeDelete.Text == "Лабораторная") //!!!
                    if (cbTypeDelete.Text == "Аудиторная") 
                    {
                        string queryIdLabList = @"select Work.id_Lab from Work where Work.Name_Work = @Name_Work and Work.Var = @Var and Work.id_Lab is not null";
                        SQLiteCommand commandIdLabList = new SQLiteCommand(queryIdLabList, dataBase.sConnect);
                        commandIdLabList.Parameters.Add(new SQLiteParameter("@Name_Work", Name_Work));
                        commandIdLabList.Parameters.Add(new SQLiteParameter("@Var", Var_Work));
                        var Reader = commandIdLabList.ExecuteReader();

                        cbIdLabSRSDelete.Items.Clear();
                        while (Reader.Read())
                        {
                            cbIdLabSRSDelete.Items.Add(Reader["id_Lab"]);
                        }
                    }
                    else //значит выбрана СРС
                    {
                        string queryIdSRSList = @"select Work.id_Srs from Work where Work.Name_Work = @Name_Work and Work.Var = @Var and Work.id_Srs is not null";
                        SQLiteCommand commandIdSRSList = new SQLiteCommand(queryIdSRSList, dataBase.sConnect);
                        commandIdSRSList.Parameters.Add(new SQLiteParameter("@Name_Work", Name_Work));
                        commandIdSRSList.Parameters.Add(new SQLiteParameter("@Var", Var_Work));
                        var Reader = commandIdSRSList.ExecuteReader();

                        cbIdLabSRSDelete.Items.Clear();
                        while (Reader.Read())
                        {
                            cbIdLabSRSDelete.Items.Add(Reader["id_Srs"]);
                        }
                    }
                }
            }
        }

        private void cbIdLabSRSDelete_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbIdLabSRSDelete.SelectedIndex != -1) //если выбран тип работы
            {
                tbTextDelete.Text = "";
                using (var dataBase = new MyDatabase())
                {       //отображение текста           
                    string queryGetText = @"select Text_Work.Text from Text_Work where Text_Work.id = @id";
                    SQLiteCommand commandGetText = new SQLiteCommand(queryGetText, dataBase.sConnect);
                    commandGetText.Parameters.Add(new SQLiteParameter("@id", Convert.ToInt32(cbIdLabSRSDelete.Text)));
                    tbTextDelete.Text = (commandGetText.ExecuteScalar()).ToString();

                    //отображение входных данных и заполнение list'a input
                    input = new List<string>();
                    string QueryInput = @"SELECT Input_Data FROM Examination where id_Text_Work=:id_Text_Work ";
                    SQLiteCommand commandQueryInput = new SQLiteCommand(QueryInput, dataBase.sConnect);
                    commandQueryInput.Parameters.Add(new SQLiteParameter("id_Text_Work", Convert.ToInt32(cbIdLabSRSDelete.Text)));

                    var reader = commandQueryInput.ExecuteReader();
                    while (reader.Read())
                    {
                        input.Add((string)reader["Input_Data"]);
                    }
                    if (input.Count >0)// может быть пустой запрос, если на форме добавления не внесли вх/вых данные
                    {
                        tbInputDataDelete.Text = input[0]; //заполнили первым значением
                        examination_iterator = 0;
                    }
                    else tbInputDataDelete.Text = "Нет данных";

                    //отображение выходных данных и заполнение list'a output
                    output = new List<string>();
                    string QueryOutput = @"SELECT Output_Data FROM Examination where id_Text_Work=:id_Text_Work ";
                    SQLiteCommand commandQueryOutput = new SQLiteCommand(QueryOutput, dataBase.sConnect);
                    commandQueryOutput.Parameters.Add(new SQLiteParameter("id_Text_Work", Convert.ToInt32(cbIdLabSRSDelete.Text)));

                    var readerOutput = commandQueryOutput.ExecuteReader();
                    while (readerOutput.Read())
                    {
                        output.Add((string)readerOutput["Output_Data"]);
                    }
                    if (output.Count() > 0)
                    {
                        tbOutputDataDelete.Text = output[0];//заполнили первым значением

                    }
                    else tbOutputDataDelete.Text = "Нет данных";
                }
            }
        }

        private void btnLeftDelete_Click(object sender, EventArgs e)
        {
            ScrollToLeft(tbInputDataDelete, tbOutputDataDelete);
        }
        private void btnRightDelete_Click(object sender, EventArgs e)
        {
            ScrollToRight(tbInputDataDelete, tbOutputDataDelete);
        }
        public void ScrollToLeft(TextBox InputData, TextBox OutputData)
        {
            if (examination_iterator > 0)//если есть след. запись в списке input(соответственно и в output)
            {
                InputData.Text = input[examination_iterator - 1];
                OutputData.Text = output[examination_iterator - 1];
                examination_iterator--;
            }
        }
        public void ScrollToRight(TextBox InputData, TextBox OutputData)
        {
            if (examination_iterator < input.Count - 1)//если есть след. запись в списке input(соответственно и в output)
            {
                InputData.Text = input[examination_iterator + 1];
                OutputData.Text = output[examination_iterator + 1];
                examination_iterator++;
            }
        }

        private void pbDeleteExaminationData_Click(object sender, EventArgs e)
        {
            if (tbInputDataDelete.Text != "" && tbOutputDataDelete.Text != "")
            {
                using (var dataBase = new MyDatabase())
                {
                    string QueryDeleteExamination = @"DELETE FROM Examination WHERE id_Text_Work=:id_Text_Work AND Input_Data=:Input_Data AND Output_Data=:Output_Data";
                    SQLiteCommand commandQueryDeleteExamination = new SQLiteCommand(QueryDeleteExamination, dataBase.sConnect);
                    commandQueryDeleteExamination.Parameters.Add(new SQLiteParameter("id_Text_Work", Convert.ToInt32(cbIdLabSRSDelete.Text)));
                    commandQueryDeleteExamination.Parameters.Add(new SQLiteParameter("Input_Data", tbInputDataDelete.Text)); 
                    commandQueryDeleteExamination.Parameters.Add(new SQLiteParameter("Output_Data", tbOutputDataDelete.Text)); 
                    commandQueryDeleteExamination.ExecuteNonQuery();
                    MessageBox.Show("Набор данных удален");

                    //убрали из списка
                    output.Remove(tbOutputDataDelete.Text);
                    input.Remove(tbInputDataDelete.Text);

                    MessageBox.Show(examination_iterator.ToString());
                    //отображение нового в боксах
                    if(examination_iterator > 0)
                    {
                        tbInputDataDelete.Text = input[examination_iterator-1];
                        tbOutputDataDelete.Text = output[examination_iterator-1];
                        examination_iterator--;
                    }
                    else if (input.Count() != 0)
                    {
                        tbInputDataDelete.Text = input[examination_iterator + 1];
                        tbOutputDataDelete.Text = output[examination_iterator + 1];
                        examination_iterator++;
                    }
                    else 
                    {
                        tbInputDataDelete.Text = "Нет данных";
                        tbOutputDataDelete.Text = "Нет данных";
                    }
                }
            }
            else MessageBox.Show("Выберите данные для удаления");
        }

     
       //удалить определенную работу
        private void pbDeleteLabSRS_Click(object sender, EventArgs e)
        {
            if (cbIdLabSRSDelete.Text == "") return;
            using (var dataBase = new MyDatabase())
            {
                //Заменили в таблице work нужное значение на NULL
                //if (cbTypeDelete.Text == "Лабораторная" && cbIdLabSRSDelete.Text!="") //!!!
                if (cbTypeDelete.Text == "Аудиторная" && cbIdLabSRSDelete.Text != "")
                {
                    string QueryDeleteLabSRSFromWork = @"UPDATE Work SET id_Lab=null WHERE id_Lab=:id_Lab";
                    SQLiteCommand commandQueryDeleteLabSRSFromWork = new SQLiteCommand(QueryDeleteLabSRSFromWork, dataBase.sConnect);
                    commandQueryDeleteLabSRSFromWork.Parameters.Add(new SQLiteParameter("id_Lab", Convert.ToInt32(cbIdLabSRSDelete.Text)));
                    commandQueryDeleteLabSRSFromWork.ExecuteNonQuery();
                }
                else //значит СРС
                {
                    string QueryDeleteLabSRSFromWork = @"UPDATE Work SET id_SRS=null WHERE id_SRS=:id_SRS";
                    SQLiteCommand commandQueryDeleteLabSRSFromWork = new SQLiteCommand(QueryDeleteLabSRSFromWork, dataBase.sConnect);
                    commandQueryDeleteLabSRSFromWork.Parameters.Add(new SQLiteParameter("id_SRS", Convert.ToInt32(cbIdLabSRSDelete.Text)));
                    commandQueryDeleteLabSRSFromWork.ExecuteNonQuery();
                }
                 MessageBox.Show("В табл. Work стал NULL");

                 //написано неправильно и реализовано дальше в DeleteEmptyRecords
                //удалили из таблицы Text_Work
                //string QueryDeleteFromText_Work = @"DELETE FROM Text_Work WHERE id=:id";
                //SQLiteCommand commandQueryDeleteFromText_Work = new SQLiteCommand(QueryDeleteFromText_Work, dataBase.sCon);
                //commandQueryDeleteFromText_Work.Parameters.Add(new SQLiteParameter("id", Convert.ToInt32(cbIdLabSRSDelete.Text)));
                //commandQueryDeleteFromText_Work.ExecuteNonQuery();
                //MessageBox.Show("Запись из таблицы Text_Work удалена");

                //удаляем из Examination ВСЕ записи с этим Id
                string QueryDeleteFromExamination = @"DELETE FROM Examination WHERE id_Text_Work=:id_Text_Work";
                SQLiteCommand commandQueryDeleteFromExamination = new SQLiteCommand(QueryDeleteFromExamination, dataBase.sConnect);
                commandQueryDeleteFromExamination.Parameters.Add(new SQLiteParameter("id_Text_Work", Convert.ToInt32(cbIdLabSRSDelete.Text)));
                commandQueryDeleteFromExamination.ExecuteNonQuery();
                MessageBox.Show("Записи из таблицы Examination удалены");

                DeleteEmptyRecords(); //удаляем пустые строчки (мусор)
        }
            tbInputDataDelete.Text ="";
            tbOutputDataDelete.Text = "";
            tbTextDelete.Text = "";

            int index = cbIdLabSRSDelete.SelectedIndex;
            cbIdLabSRSDelete.Items.Remove(cbIdLabSRSDelete.SelectedItem);
            if (cbWorkEdit.SelectedIndex == cbWorkDelete.SelectedIndex && cbVarEdit.SelectedIndex == cbVarDelete.SelectedIndex && cbTypeEdit.SelectedIndex == cbTypeDelete.SelectedIndex)
            {
                cbIdLabSRS.Items.RemoveAt(index);
            }

           
}

        //удаление варианта
        private void pbDeleteVar_Click(object sender, EventArgs e)
        {
            idLabSRSToRemove = new List<int>();
            //найдем все нужные id лаб и срс для удаления
            if (cbVarDelete.Text == "") return;
            using (var dataBase = new MyDatabase())
            {
                //ищем id_Lab, которые нужно будет удалить
                string queryGetIdsLab = @"select id_Lab FROM Work WHERE Name_Work=:Name_Work AND Var=:Var AND id_Lab is not null ";
                SQLiteCommand commandqueryGetIdsLab = new SQLiteCommand(queryGetIdsLab, dataBase.sConnect);
                commandqueryGetIdsLab.Parameters.Add(new SQLiteParameter("Name_Work", cbWorkDelete.Text));
                commandqueryGetIdsLab.Parameters.Add(new SQLiteParameter("Var", cbVarDelete.Text));
                var Reader = commandqueryGetIdsLab.ExecuteReader();
                while (Reader.Read())
                {
                    idLabSRSToRemove.Add(Convert.ToInt32(Reader["id_Lab"])); //добавили в коллекцию
                }

                //ищем id_SRS, которые нужно будет удалить
                string queryGetIdsSRS = @"select id_SRS FROM Work WHERE Name_Work=:Name_Work AND Var=:Var AND id_SRS is not null ";
                SQLiteCommand commandqueryGetIdsSRS = new SQLiteCommand(queryGetIdsSRS, dataBase.sConnect);
                commandqueryGetIdsSRS.Parameters.Add(new SQLiteParameter("Name_Work", cbWorkDelete.Text));
                commandqueryGetIdsSRS.Parameters.Add(new SQLiteParameter("Var", cbVarDelete.Text));
                var ReaderSRS = commandqueryGetIdsSRS.ExecuteReader();
                while (ReaderSRS.Read())
                {
                    idLabSRSToRemove.Add(Convert.ToInt32(ReaderSRS["id_SRS"])); //добавили в коллекцию
                }

                MessageBox.Show("idLabSRSTo Remove: ");
                string temp="";
                for (int i = 0; i < idLabSRSToRemove.Count(); i++) temp=temp+Convert.ToString(idLabSRSToRemove[i])+" ";
                MessageBox.Show(temp);

                //удалили строки с нужным набором (работа+var) из таблицы Work
                string QueryDeleteFromWork = @"DELETE FROM Work WHERE Name_Work=:Name_Work AND Var=:Var";
                SQLiteCommand commandQueryDeleteFromWork = new SQLiteCommand(QueryDeleteFromWork, dataBase.sConnect);
                commandQueryDeleteFromWork.Parameters.Add(new SQLiteParameter("Name_Work", cbWorkDelete.Text));
                commandQueryDeleteFromWork.Parameters.Add(new SQLiteParameter("Var", cbVarDelete.Text));
                commandQueryDeleteFromWork.ExecuteNonQuery();
                MessageBox.Show("Все нужные var'ы удалены из Work");   

                //удаляем все нужные работы из Text_Work
                for (int i = 0; i < idLabSRSToRemove.Count(); i++)
                {
                    string QueryDeleteFromText_Work = @"DELETE FROM Text_Work WHERE id=:id";
                    SQLiteCommand commandQueryDeleteFromText_Work = new SQLiteCommand(QueryDeleteFromText_Work, dataBase.sConnect);
                    commandQueryDeleteFromText_Work.Parameters.Add(new SQLiteParameter("id", idLabSRSToRemove[i]));
                    commandQueryDeleteFromText_Work.ExecuteNonQuery();
                   
                }
                MessageBox.Show("Все нужные записи удалены из text_Work");

                //удаляем все нужные записи из Examination
                for (int i = 0; i < idLabSRSToRemove.Count(); i++)
                {
                    string QueryDeleteFromExamination = @"DELETE FROM Examination WHERE id_Text_Work=:id_Text_Work";
                    SQLiteCommand commandQueryDeleteFromExamination = new SQLiteCommand(QueryDeleteFromExamination, dataBase.sConnect);
                    commandQueryDeleteFromExamination.Parameters.Add(new SQLiteParameter("id_Text_Work", idLabSRSToRemove[i]));
                    commandQueryDeleteFromExamination.ExecuteNonQuery();
                    
                }
                MessageBox.Show("Все нужные записи удалены из Examination");

            }
            cbIdLabSRSDelete.SelectedIndex = -1; //почистили 4-й бокс
            tbInputDataDelete.Text = "";
            tbOutputDataDelete.Text = "";
            tbTextDelete.Text = ""; //почистили текст
            idLabSRSToRemove.Clear(); //почистили лист
            int index = cbVarDelete.SelectedIndex;
            cbVarDelete.Items.Remove(cbVarDelete.SelectedItem);
            if (cbWorkEdit.SelectedIndex == cbWorkDelete.SelectedIndex)
            {
                cbVarEdit.Items.RemoveAt(index);
            }
            if (cbWork.SelectedIndex == cbWorkDelete.SelectedIndex)
            {
                cbVariant.Items.RemoveAt(index);
            }

        }

        //удаление темы
        private void pbDeleteWork_Click(object sender, EventArgs e)
        {
            if (cbWorkDelete.Text == "") return;
              //найдем все нужные id лаб и срс для удаления
            using (var dataBase = new MyDatabase())
            {
                idLabSRSToRemove = new List<int>();
                //ищем id_Lab, которые нужно будет удалить
                string queryGetIdsLab = @"select id_Lab FROM Work WHERE Name_Work=:Name_Work AND id_Lab is not null ";
                SQLiteCommand commandqueryGetIdsLab = new SQLiteCommand(queryGetIdsLab, dataBase.sConnect);
                commandqueryGetIdsLab.Parameters.Add(new SQLiteParameter("Name_Work", cbWorkDelete.Text));
                var Reader = commandqueryGetIdsLab.ExecuteReader();
                while (Reader.Read())
                {
                    idLabSRSToRemove.Add(Convert.ToInt32(Reader["id_Lab"])); //добавили в коллекцию
                }

                //ищем id_SRS, которые нужно будет удалить
                string queryGetIdsSRS = @"select id_SRS FROM Work WHERE Name_Work=:Name_Work AND id_SRS is not null ";
                SQLiteCommand commandqueryGetIdsSRS = new SQLiteCommand(queryGetIdsSRS, dataBase.sConnect);
                commandqueryGetIdsSRS.Parameters.Add(new SQLiteParameter("Name_Work", cbWorkDelete.Text));
                var ReaderSRS = commandqueryGetIdsSRS.ExecuteReader();
                while (ReaderSRS.Read())
                {
                    idLabSRSToRemove.Add(Convert.ToInt32(ReaderSRS["id_SRS"])); //добавили в коллекцию
                }

                MessageBox.Show("Будут удалены записи со след. id: (id_SRS && id_Lab) ");
                for (int i = 0; i < idLabSRSToRemove.Count(); i++) MessageBox.Show(idLabSRSToRemove[i].ToString());


                //удалили строки с нужным work'ом из таблицы Work
                string QueryDeleteFromWork = @"DELETE FROM Work WHERE Name_Work=:Name_Work";
                SQLiteCommand commandQueryDeleteFromWork = new SQLiteCommand(QueryDeleteFromWork, dataBase.sConnect);
                commandQueryDeleteFromWork.Parameters.Add(new SQLiteParameter("Name_Work", cbWorkDelete.Text));
                commandQueryDeleteFromWork.ExecuteNonQuery();
                MessageBox.Show("Все нужные work'и удалены из Work");

                //удаляем все нужные работы из Text_Work
                for (int i = 0; i < idLabSRSToRemove.Count(); i++)
                {
                    string QueryDeleteFromText_Work = @"DELETE FROM Text_Work WHERE id=:id";
                    SQLiteCommand commandQueryDeleteFromText_Work = new SQLiteCommand(QueryDeleteFromText_Work, dataBase.sConnect);
                    commandQueryDeleteFromText_Work.Parameters.Add(new SQLiteParameter("id", idLabSRSToRemove[i]));
                    commandQueryDeleteFromText_Work.ExecuteNonQuery(); 
                }
                MessageBox.Show("Все нужные записи удалены из text_Work");

                //удаляем все нужные записи из Examination
                for (int i = 0; i < idLabSRSToRemove.Count(); i++)
                {
                    string QueryDeleteFromExamination = @"DELETE FROM Examination WHERE id_Text_Work=:id_Text_Work";
                    SQLiteCommand commandQueryDeleteFromExamination = new SQLiteCommand(QueryDeleteFromExamination, dataBase.sConnect);
                    commandQueryDeleteFromExamination.Parameters.Add(new SQLiteParameter("id_Text_Work", idLabSRSToRemove[i]));
                    commandQueryDeleteFromExamination.ExecuteNonQuery();      
                }
                MessageBox.Show("Все нужные записи удалены из Examination");
                idLabSRSToRemove.Clear(); //почистили лист

                tbInputDataDelete.Text = "";
                tbOutputDataDelete.Text = "";
                tbTextDelete.Text = "";

                int index = cbWorkDelete.SelectedIndex;
                cbWorkDelete.Items.Remove(cbWorkDelete.SelectedItem);
                cbWorkEdit.Items.RemoveAt(index);
                cbWork.Items.RemoveAt(index);
            }
        }   
    
        public void DeleteEmptyRecords()
        {
          using (var dataBase = new MyDatabase())
          {
              idLabSRSToRemove = new List<int>();
              //ищем записи с пустыми строками(пустым id_Lab и пустым id_SRS)
              string queryGetEmptyRecords = @"select id FROM Work WHERE id_Lab is null AND id_SRS is null ";
              SQLiteCommand commandqueryGetEmptyRecords = new SQLiteCommand(queryGetEmptyRecords, dataBase.sConnect);
              var Reader = commandqueryGetEmptyRecords.ExecuteReader();
              while (Reader.Read())
              {
                  idLabSRSToRemove.Add(Convert.ToInt32(Reader["id"])); //добавили в коллекцию
              }

              MessageBox.Show("Будут удалены со след. id: (очистка пустых в ворке) ");
              for (int i = 0; i < idLabSRSToRemove.Count(); i++) MessageBox.Show(idLabSRSToRemove[i].ToString());

              //удаляем строки
              for (int i = 0; i < idLabSRSToRemove.Count(); i++)
              {
                  string queryDeleteEmptyRecords = @"DELETE FROM Work WHERE id=:id";
                  SQLiteCommand commandqueryDeleteEmptyRecords = new SQLiteCommand(queryDeleteEmptyRecords, dataBase.sConnect);
                  commandqueryDeleteEmptyRecords.Parameters.Add(new SQLiteParameter("id", idLabSRSToRemove[i]));
                  commandqueryDeleteEmptyRecords.ExecuteNonQuery();
              }
              idLabSRSToRemove.Clear(); //почистили лист
          }
        }

        //отображение авто-заполнения текста в добавлении
        private void cbVariant_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillText(cbWork.Text, cbVariant.Text, tbTaskText);
        }

        private void pbCraftText_Click(object sender, EventArgs e)
        {
            FillText(cbWork.Text, cbVariant.Text, tbTaskText);
        }
    } 
}
