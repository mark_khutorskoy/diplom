﻿namespace Programm
{
    partial class TV
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TV));
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabTheory = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ClassesTreeView = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSendWork = new System.Windows.Forms.Button();
            this.btnBack1 = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFIO = new System.Windows.Forms.Label();
            this.lblGroup = new System.Windows.Forms.Label();
            this.axAcroPDF = new AxAcroPDFLib.AxAcroPDF();
            this.tabLab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.openIDE = new System.Windows.Forms.Button();
            this.btnBack2 = new System.Windows.Forms.Button();
            this.axAcroPDFLab = new AxAcroPDFLib.AxAcroPDF();
            this.tabTest = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.TVtests = new System.Windows.Forms.TreeView();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAddTest = new System.Windows.Forms.Button();
            this.btnBack3 = new System.Windows.Forms.Button();
            this.btnDeleteTest = new System.Windows.Forms.Button();
            this.pbReload = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tpHover = new System.Windows.Forms.ToolTip(this.components);
            this.tabs.SuspendLayout();
            this.tabTheory.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF)).BeginInit();
            this.tabLab.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDFLab)).BeginInit();
            this.tabTest.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbReload)).BeginInit();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabTheory);
            this.tabs.Controls.Add(this.tabLab);
            this.tabs.Controls.Add(this.tabTest);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabs.Location = new System.Drawing.Point(0, 0);
            this.tabs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(1351, 633);
            this.tabs.TabIndex = 0;
            this.tabs.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabs_Selecting);
            // 
            // tabTheory
            // 
            this.tabTheory.Controls.Add(this.tableLayoutPanel1);
            this.tabTheory.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabTheory.Location = new System.Drawing.Point(4, 29);
            this.tabTheory.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabTheory.Name = "tabTheory";
            this.tabTheory.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabTheory.Size = new System.Drawing.Size(1343, 600);
            this.tabTheory.TabIndex = 0;
            this.tabTheory.Text = "Теория";
            this.tabTheory.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 335F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.ClassesTreeView, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.axAcroPDF, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.10891F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.89109F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1335, 592);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // ClassesTreeView
            // 
            this.ClassesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClassesTreeView.Location = new System.Drawing.Point(24, 4);
            this.ClassesTreeView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ClassesTreeView.Name = "ClassesTreeView";
            this.ClassesTreeView.Size = new System.Drawing.Size(327, 519);
            this.ClassesTreeView.TabIndex = 9;
            this.ClassesTreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.ClassesTreeView_NodeMouseClick);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.Controls.Add(this.btnSendWork, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnBack1, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCopy, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(358, 529);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(954, 61);
            this.tableLayoutPanel2.TabIndex = 13;
            // 
            // btnSendWork
            // 
            this.btnSendWork.Location = new System.Drawing.Point(357, 2);
            this.btnSendWork.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSendWork.Name = "btnSendWork";
            this.btnSendWork.Size = new System.Drawing.Size(184, 55);
            this.btnSendWork.TabIndex = 18;
            this.btnSendWork.Text = "Сдать работу";
            this.btnSendWork.UseVisualStyleBackColor = true;
            this.btnSendWork.Click += new System.EventHandler(this.BtnSendWork_Click_1);
            // 
            // btnBack1
            // 
            this.btnBack1.Location = new System.Drawing.Point(757, 2);
            this.btnBack1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBack1.Name = "btnBack1";
            this.btnBack1.Size = new System.Drawing.Size(184, 55);
            this.btnBack1.TabIndex = 16;
            this.btnBack1.Text = "Назад";
            this.btnBack1.UseVisualStyleBackColor = true;
            this.btnBack1.Click += new System.EventHandler(this.Back);
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(557, 2);
            this.btnCopy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(184, 55);
            this.btnCopy.TabIndex = 14;
            this.btnCopy.Text = "Собрать рабочий\r\n материал";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.BtnCopy_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.lblFIO, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblGroup, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(23, 529);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(329, 61);
            this.tableLayoutPanel3.TabIndex = 14;
            // 
            // lblFIO
            // 
            this.lblFIO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFIO.AutoSize = true;
            this.lblFIO.Location = new System.Drawing.Point(3, 10);
            this.lblFIO.Name = "lblFIO";
            this.lblFIO.Size = new System.Drawing.Size(264, 20);
            this.lblFIO.TabIndex = 15;
            this.lblFIO.Text = "Давыдова Диана Эдуардовна";
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.Location = new System.Drawing.Point(3, 30);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(85, 20);
            this.lblGroup.TabIndex = 16;
            this.lblGroup.Text = "ПМИ - 3,4";
            // 
            // axAcroPDF
            // 
            this.axAcroPDF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axAcroPDF.Enabled = true;
            this.axAcroPDF.Location = new System.Drawing.Point(358, 2);
            this.axAcroPDF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.axAcroPDF.Name = "axAcroPDF";
            this.axAcroPDF.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAcroPDF.OcxState")));
            this.axAcroPDF.Size = new System.Drawing.Size(954, 523);
            this.axAcroPDF.TabIndex = 15;
            // 
            // tabLab
            // 
            this.tabLab.Controls.Add(this.tableLayoutPanel4);
            this.tabLab.Location = new System.Drawing.Point(4, 29);
            this.tabLab.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabLab.Name = "tabLab";
            this.tabLab.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabLab.Size = new System.Drawing.Size(1343, 600);
            this.tabLab.TabIndex = 1;
            this.tabLab.Text = "Лабораторная";
            this.tabLab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.axAcroPDFLab, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1335, 592);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189F));
            this.tableLayoutPanel7.Controls.Add(this.openIDE, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.btnBack2, 2, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(23, 514);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1288, 66);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // openIDE
            // 
            this.openIDE.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.openIDE.AutoSize = true;
            this.openIDE.Location = new System.Drawing.Point(914, 4);
            this.openIDE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.openIDE.Name = "openIDE";
            this.openIDE.Size = new System.Drawing.Size(181, 58);
            this.openIDE.TabIndex = 2;
            this.openIDE.Text = "Открыть среду \r\nразработки";
            this.openIDE.UseVisualStyleBackColor = true;
            this.openIDE.Click += new System.EventHandler(this.OpenIDE_Click);
            // 
            // btnBack2
            // 
            this.btnBack2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnBack2.AutoSize = true;
            this.btnBack2.Location = new System.Drawing.Point(1102, 4);
            this.btnBack2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBack2.Name = "btnBack2";
            this.btnBack2.Size = new System.Drawing.Size(183, 58);
            this.btnBack2.TabIndex = 16;
            this.btnBack2.Text = "Назад";
            this.btnBack2.UseVisualStyleBackColor = true;
            this.btnBack2.Click += new System.EventHandler(this.Back);
            // 
            // axAcroPDFLab
            // 
            this.axAcroPDFLab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axAcroPDFLab.Enabled = true;
            this.axAcroPDFLab.Location = new System.Drawing.Point(23, 2);
            this.axAcroPDFLab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.axAcroPDFLab.Name = "axAcroPDFLab";
            this.axAcroPDFLab.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAcroPDFLab.OcxState")));
            this.axAcroPDFLab.Size = new System.Drawing.Size(1288, 508);
            this.axAcroPDFLab.TabIndex = 16;
            // 
            // tabTest
            // 
            this.tabTest.Controls.Add(this.tableLayoutPanel5);
            this.tabTest.Location = new System.Drawing.Point(4, 29);
            this.tabTest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabTest.Name = "tabTest";
            this.tabTest.Size = new System.Drawing.Size(1343, 600);
            this.tabTest.TabIndex = 2;
            this.tabTest.Text = "Тесты";
            this.tabTest.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Controls.Add(this.TVtests, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.rtbDescription, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.pbReload, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1343, 600);
            this.tableLayoutPanel5.TabIndex = 26;
            // 
            // TVtests
            // 
            this.TVtests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TVtests.Location = new System.Drawing.Point(23, 2);
            this.TVtests.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TVtests.Name = "TVtests";
            this.TVtests.Size = new System.Drawing.Size(394, 506);
            this.TVtests.TabIndex = 0;
            this.TVtests.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TVtests_NodeMouseClick);
            // 
            // rtbDescription
            // 
            this.rtbDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbDescription.Location = new System.Drawing.Point(423, 2);
            this.rtbDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(897, 506);
            this.rtbDescription.TabIndex = 1;
            this.rtbDescription.Text = "";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189F));
            this.tableLayoutPanel6.Controls.Add(this.btnAddTest, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnBack3, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnDeleteTest, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(423, 512);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(897, 66);
            this.tableLayoutPanel6.TabIndex = 2;
            // 
            // btnAddTest
            // 
            this.btnAddTest.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnAddTest.Location = new System.Drawing.Point(333, 4);
            this.btnAddTest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddTest.Name = "btnAddTest";
            this.btnAddTest.Size = new System.Drawing.Size(183, 57);
            this.btnAddTest.TabIndex = 18;
            this.btnAddTest.Text = "Добавить тест";
            this.btnAddTest.UseVisualStyleBackColor = true;
            this.btnAddTest.Click += new System.EventHandler(this.btnAddTest_Click);
            // 
            // btnBack3
            // 
            this.btnBack3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnBack3.AutoSize = true;
            this.btnBack3.Location = new System.Drawing.Point(711, 4);
            this.btnBack3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBack3.Name = "btnBack3";
            this.btnBack3.Size = new System.Drawing.Size(183, 57);
            this.btnBack3.TabIndex = 17;
            this.btnBack3.Text = "Назад";
            this.btnBack3.UseVisualStyleBackColor = true;
            this.btnBack3.Click += new System.EventHandler(this.Back);
            // 
            // btnDeleteTest
            // 
            this.btnDeleteTest.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnDeleteTest.Location = new System.Drawing.Point(522, 4);
            this.btnDeleteTest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDeleteTest.Name = "btnDeleteTest";
            this.btnDeleteTest.Size = new System.Drawing.Size(183, 57);
            this.btnDeleteTest.TabIndex = 0;
            this.btnDeleteTest.Text = "Удалить тест";
            this.btnDeleteTest.UseVisualStyleBackColor = true;
            this.btnDeleteTest.Click += new System.EventHandler(this.btnDeleteTest_Click);
            // 
            // pbReload
            // 
            this.pbReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbReload.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pbReload.Image = global::Programm.Properties.Resources.reload;
            this.pbReload.Location = new System.Drawing.Point(390, 512);
            this.pbReload.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbReload.Name = "pbReload";
            this.pbReload.Size = new System.Drawing.Size(27, 25);
            this.pbReload.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbReload.TabIndex = 3;
            this.pbReload.TabStop = false;
            this.tpHover.SetToolTip(this.pbReload, "Обновить дерево");
            this.pbReload.Click += new System.EventHandler(this.pbReload_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "XML Files|*.xml";
            this.openFileDialog1.Title = "\"Select XML File\"";
            // 
            // TV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1351, 633);
            this.Controls.Add(this.tabs);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "TV";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Обучение";
            this.tabs.ResumeLayout(false);
            this.tabTheory.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF)).EndInit();
            this.tabLab.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDFLab)).EndInit();
            this.tabTest.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbReload)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TreeView ClassesTreeView;
        private System.Windows.Forms.TabPage tabLab;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TabPage tabTest;
        private System.Windows.Forms.Button openIDE;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label lblFIO;
        private System.Windows.Forms.TabPage tabTheory;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private AxAcroPDFLib.AxAcroPDF axAcroPDF;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TreeView TVtests;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button btnDeleteTest;
        private System.Windows.Forms.Button btnBack1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button btnSendWork;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnBack2;
        private System.Windows.Forms.Button btnBack3;
        private System.Windows.Forms.Button btnAddTest;
        private System.Windows.Forms.PictureBox pbReload;
        private System.Windows.Forms.ToolTip tpHover;
        private AxAcroPDFLib.AxAcroPDF axAcroPDFLab;
    }
}

