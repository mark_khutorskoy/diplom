﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
using System.Diagnostics;

namespace Programm
{
    
    public partial class TestTasks : Form
    {
        string test_result = "";
        int curUserId; //id текущего юезра
        
        public bool flaglab2 = false;
        public bool flagsrs2 = false;

        public TestTasks(bool flaglab, bool flagsrs, int _curUserId)
        {
            InitializeComponent();
            curUserId = _curUserId;
            //btOk.Enabled = false;
            SetHomeWorkLab(cbNumlab);               
            cbNumlab.Enabled = false;
            cbNumVar.Enabled = false;
            flaglab2 = flaglab;
            flagsrs2 = flagsrs;
        }

        public string pathToUserCs = null;
        public string PathForTest = "C:\\Temp\\Tests\\";

        public void SetHomeWorkLab(ComboBox x)
        {
            using (var dataBase = new MyDatabase())
            {
                string queryCount = @"select distinct Name_Work from Work";
                SQLiteCommand commandCount = new SQLiteCommand(queryCount, dataBase.sConnect);
                var Reader = commandCount.ExecuteReader();
                x.Items.Clear();
                while (Reader.Read())
                {
                    x.Items.Add(Reader["Name_Work"]);
                }
            }
        }

        //Выбор раздела для сдачи
        private void cbNumlab_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbNumVar.Items.Clear();
            cbNumVar.Text = "";
            if (flagsrs2)
            {
                
                if (cbNumlab.SelectedIndex != -1)
                {
                    string Name_Lab = renamesrs(cbNumlab.SelectedItem.ToString());
                    using (var dataBase = new MyDatabase())
                    {
                        string queryCount = @"select distinct Work.var from Work where Work.Name_Work = @Name_Lab and id_srs IS NOT NULL ";
                        SQLiteCommand commandVar = new SQLiteCommand(queryCount, dataBase.sConnect);
                        commandVar.Parameters.Add(new SQLiteParameter("@Name_Lab", Name_Lab));
                        var Reader = commandVar.ExecuteReader();
                        cbNumVar.Items.Clear();
                        while (Reader.Read())
                        {
                            string rename = Reader["var"].ToString();
                            string[] ren = rename.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                            for (int i = 0; i < ren.Length; i++)
                            {
                                if (ren[i] == "Лабораторная")
                                {
                                    ren[i] = "СРС";
                                }
                            }
                            cbNumVar.Items.Add(string.Join(" ",ren));
                        }
                    }

                    if (pathToUserCs != null && cbNumVar.Text != null && cbNumlab.Text != null)
                    {
                        btOk.Enabled = true;
                    }
                }
            }
            if (flaglab2)
            { 
                if (cbNumlab.SelectedIndex != -1)
                {
                    string Name_Lab = cbNumlab.SelectedItem.ToString();
                    using (var dataBase = new MyDatabase())
                    {
                        string queryCount = @"select distinct Work.var from Work where Work.Name_Work = @Name_Lab and id_lab IS NOT NULL ";
                        SQLiteCommand commandVar = new SQLiteCommand(queryCount, dataBase.sConnect);
                        commandVar.Parameters.Add(new SQLiteParameter("@Name_Lab", Name_Lab));
                        var Reader = commandVar.ExecuteReader();
                        cbNumVar.Items.Clear();
                        while (Reader.Read())
                        {
                            cbNumVar.Items.Add(Reader["var"]);
                        }
                    }

                    if (pathToUserCs != null && cbNumVar.Text != null && cbNumlab.Text != null)
                    {
                        btOk.Enabled = true;
                    }
                }
            }
        }

        //Выбираем папку в которой лежит программа студента
        private void btRepozit_Click(object sender, EventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pathToUserCs = dialog.SelectedPath;
                cbNumlab.Enabled = true;
                cbNumVar.Enabled = true;
            }
        }

        int ColTestsLab()//Возвращает количество тестов id_Lab выбранного раздела, лабораторной и выбранного варианта
        {
            string Name_Lab = cbNumlab.Text; //хранит название выбранного раздела
            string Var = cbNumVar.Text;//хранит выбранный вариант и лабораторную
            using (var dataBase = new MyDatabase())
            {

                string queryCount2 = @"select COUNT(Output_Data) from 
                                Examination join Text_Work on Examination.id_Text_Work = Text_Work.id  
                                join  Work on Work.id_lab = Text_Work.id
                                where Name_Work=@Name_Lab and Work.var=@var ";

                SQLiteCommand commandInput2 = new SQLiteCommand(queryCount2, dataBase.sConnect);
                commandInput2.Parameters.Add(new SQLiteParameter("@Name_Lab", Name_Lab));
                commandInput2.Parameters.Add(new SQLiteParameter("@var", Var));
                int ColTest = Convert.ToInt32(commandInput2.ExecuteScalar());
                return (ColTest);
            }
        }
        int ColTestsSrs()//Возвращает количество тестов у выбранного раздела, срс и выбранного варианта
        {
            string Name_Lab = cbNumlab.Text;
            string Var = renamesrs(cbNumVar.Text);
            using (var dataBase = new MyDatabase())
            {

                string queryCount2 = @"select COUNT(Output_Data) from 
                                Examination join Text_Work on Examination.id_Text_Work = Text_Work.id  
                                join  Work on Work.id_Srs = Text_Work.id
                                where Name_Work=@Name_Work and Work.var=@var ";
                SQLiteCommand commandInput2 = new SQLiteCommand(queryCount2, dataBase.sConnect);
                commandInput2.Parameters.Add(new SQLiteParameter("@Name_Work", Name_Lab));
                commandInput2.Parameters.Add(new SQLiteParameter("@var", Var));
                int ColTest = Convert.ToInt32(commandInput2.ExecuteScalar());
                return (ColTest);
            }
        }

        private void CreateDirectotiesAndInpitFileLab()//Создает директорию для записи файла студентом, а также в папке Temp Input файл.
        {
            try
            {
                Directory.CreateDirectory(PathForTest);
                Directory.GetFiles(PathForTest).ToList().ForEach(file => File.Delete(file));
                
                
                if (pathToUserCs != null && cbNumVar.Text != null && cbNumlab.Text != null)
                {
                    string Name_Lab = cbNumlab.Text;
                    string Var = cbNumVar.Text;
                    using (var dataBase = new MyDatabase())
                    {
                        string queryCount = @"select Examination.Input_Data from 
                                Examination join Text_Work on Examination.id_Text_Work = Text_Work.id  
                                join  Work on Work.id_lab = Text_Work.id
                                where Name_Work=@Name_Lab and Work.var=@var Order BY  Examination.id ASC";
                        SQLiteCommand commandInput = new SQLiteCommand(queryCount, dataBase.sConnect);
                        commandInput.Parameters.Add(new SQLiteParameter("@Name_Lab", Name_Lab));
                        commandInput.Parameters.Add(new SQLiteParameter("@var", Var));
                        var Reader2 = commandInput.ExecuteReader();
                        string input = "";
                        while (Reader2.Read())
                        {
                            //array[j] = (Reader2["Input_Data"]).ToString();
                            //if (array[j] != null)
                            //       j++;
                            input += (Reader2["Input_Data"]).ToString() + "\n";
                        }
                        File.WriteAllText(PathForTest + "\\INPUT.txt", input);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CreateDirectotiesAndInpitFileSrs()//Создает директорию для записи файла студентом, а также в папке Temp Input файл.
        {
            try
            {
                if (Directory.Exists(PathForTest) == false)
                {
                    DirectoryInfo di = Directory.CreateDirectory(PathForTest);
                }

                if (pathToUserCs != null && cbNumVar.Text != null && cbNumlab.Text != null)
                {
                    string Name_Lab = cbNumlab.Text.ToString();
                    string Var = renamesrs(cbNumVar.Text);
                    using (var dataBase = new MyDatabase())
                    {
                        string queryCount = @"select Examination.Input_Data from 
                                Examination join Text_Work on Examination.id_Text_Work = Text_Work.id  
                                join  Work on Work.id_Srs = Text_Work.id
                                where Name_Work=@Name_Lab and Work.var=@var Order BY  Examination.id ASC";
                        SQLiteCommand commandInput = new SQLiteCommand(queryCount, dataBase.sConnect);
                        commandInput.Parameters.Add(new SQLiteParameter("@Name_Lab", Name_Lab));
                        commandInput.Parameters.Add(new SQLiteParameter("@var", Var));

                        SQLiteDataReader Reader2 = commandInput.ExecuteReader();
                        string input = "";
                        while (Reader2.Read())
                        {                           
                            input += (Reader2["Input_Data"]).ToString() + "\n";
                        }
                        File.WriteAllText(PathForTest + "\\INPUT.txt", input);
                    }
                }
        }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
}

        private void ExaminationOutputFileLab() //Проверка выходного файла 
        {
            int k = 0;//счетчик для количества правильных ответов
            string Name_Lab = cbNumlab.Text;
            string Var = cbNumVar.Text;
            using (var dataBase = new MyDatabase())
            {
                string queryCount = @"select Examination.Output_Data from 
                                Examination join Text_Work on Examination.id_Text_Work = Text_Work.id  
                                join  Work on Work.id_lab = Text_Work.id
                                where Name_Work=@Name_Lab and Work.var=@var Order BY  Examination.id ASC";
                SQLiteCommand commandInput = new SQLiteCommand(queryCount, dataBase.sConnect);
                commandInput.Parameters.Add(new SQLiteParameter("@Name_Lab", Name_Lab));
                commandInput.Parameters.Add(new SQLiteParameter("@var", Var));
                var Reader2 = commandInput.ExecuteReader();
                try
                {
                    int ColTest = ColTestsLab();
                    // string[] array = new string[ColTest+1];
                    string line = "";
                    //int j = 1;
                    string output = "";
                    StreamReader sr = new StreamReader(PathForTest + "\\OUTPUT.txt");
                    string Result="";
                    int NumTest = 0;
                    while (Reader2.Read() && !sr.EndOfStream)
                    {
                        //array[j] = (Reader2["Output_Data"]).ToString();
                        //string m = File.ReadAllText(myWayOk + "\\OUTPUT" + j + ".txt");            
                        //if (m == array[j]) k++;
                        //if (array[j] != null) j++;

                        output = (Reader2["Output_Data"]).ToString(); // + "\n";
                        line = sr.ReadLine();
                        NumTest++;
                        if (line == output)
                        {
                            k++;
                            Result+= "Тест " + NumTest.ToString() + " пройден"+"\n";  
                        } //Если считываемая строка равна аналогичной строке из БД(sort by asc)
                        else
                            Result += "Тест " + NumTest.ToString() + " не пройден" + "\n";
                        line = "";
                    }
                    sr.Close();
                    if (ColTest == k && ColTest != 0)
                    {
                        MessageBox.Show("Все тесты пройдены");
                    }
                    else if (ColTest > k) MessageBox.Show(Result, "Результат Тестирования", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else MessageBox.Show(Result, "Результат Тестирования", MessageBoxButtons.OK, MessageBoxIcon.Error);  

                    test_result = k.ToString() + " из " + ColTest.ToString(); //для результата в БД
                    SaveUserResult.FillTestResult(cbNumlab.Text, test_result, curUserId, "лаб");
                }
                catch
                {
                    try
                    {
                        Process.Start("name.bat");
                        // MessageBox.Show("Процесс запущен");
                        if (File.Exists(PathForTest + "\\OUTPUT.txt") == true)
                        {
                            //проверка ответов
                            ExaminationOutputFileLab();
                        }
                        else
                        {
                            MessageBox.Show("Вы указали неверный путь, либо файл OUTPUT Не существует в указанной папке");
                        }
                    }
                    catch { MessageBox.Show("Вы указали неверный путь, либо файл OUTPUT Не существует в указанной папке"); }
                }
                File.Delete("name.bat");
                cbNumlab.Items.Clear();
                cbNumlab.Text = "";
                cbNumVar.Items.Clear();
                cbNumVar.Text = "";
                try { Directory.Delete(PathForTest, true); }
                catch { }
                btOk.Enabled = false;
                SetHomeWorkLab(cbNumlab);
                cbNumlab.Enabled = false;
                cbNumVar.Enabled = false;
            }

        }
        public string renamesrs(string tbVar)
        {
            string rename = tbVar;
            string[] ren = rename.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            for (int i = 0; i < ren.Length; i++)
            {
                if (ren[i] == "СРС")
                {
                    ren[i] = "Лабораторная";
                }
            }
            string Var = (string.Join(" ", ren));
            return Var;
            
        }


        private void ExaminationOutputFileSrs() //Проверка выходного файла 
        {
            int k = 0;//счетчик для количества правильных ответов
            string Name_Lab = cbNumlab.Text;
            string Var = renamesrs(cbNumVar.Text.ToString());
            using (var dataBase = new MyDatabase())
            {
                string queryCount = @"select Examination.Output_Data from 
                                Examination join Text_Work on Examination.id_Text_Work = Text_Work.id  
                                join  Work on Work.id_Srs = Text_Work.id
                                where Name_Work=@Name_Work and Work.var=@var Order BY  Examination.id ASC";
                SQLiteCommand commandInput = new SQLiteCommand(queryCount, dataBase.sConnect);
                commandInput.Parameters.Add(new SQLiteParameter("@Name_Work", Name_Lab));
                commandInput.Parameters.Add(new SQLiteParameter("@var", Var));
                var Reader2 = commandInput.ExecuteReader();
                try
                {
                    int ColTest = ColTestsSrs();
                    string line = "";
                    //int j = 1;
                    string output = "";
                    StreamReader sr = new StreamReader(PathForTest + "\\OUTPUT.txt");
                    string Result = "";
                    int NumTest = 0;
                    while (Reader2.Read() && !sr.EndOfStream)
                    {
                        output = (Reader2["Output_Data"]).ToString(); // + "\n";
                        line = sr.ReadLine();
                        NumTest++;
                        if (line == output)
                        {
                            k++;
                            Result += "Тест " + NumTest.ToString() + " пройден" + "\n";
                        } //Если считываемая строка равна аналогичной строке из БД(sort by asc)
                        else
                            Result += "Тест " + NumTest.ToString() + " не пройден" + "\n";
                        line = "";
                    }
                    sr.Close();
                    if (ColTest == k && k!=0)
                    {
                        MessageBox.Show("Все тесты пройдены");
                    }
                    else if (ColTest > k) MessageBox.Show(Result, "Результат Тестирования", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //else if (ColTest == 0) MessageBox.Show("Укажите путь к новому файлу для сдачи");
                    else  MessageBox.Show(Result, "Результат Тестирования", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    test_result = k.ToString() + " из " + ColTest.ToString(); //для результата в БД
                    SaveUserResult.FillTestResult(cbNumlab.Text, test_result, curUserId, "срс");
                }
                catch
                {
                    try
                    {
                        Process.Start("name.bat");
                        // MessageBox.Show("Процесс запущен");
                        if (File.Exists(PathForTest + "\\OUTPUT.txt") == true)
                        {
                            //проверка ответов
                            ExaminationOutputFileSrs();
                        }
                        else
                        {
                            MessageBox.Show("Вы указали неверный путь, либо файл OUTPUT Не существует в указанной папке");
                        }
                    }
                    catch { MessageBox.Show("Вы указали неверный путь, либо файл OUTPUT Не существует в указанной папке"); }
                }
            }
            File.Delete("name.bat");
            cbNumlab.Items.Clear();
            cbNumlab.Text = "";
            cbNumVar.Items.Clear();
            cbNumVar.Text = "";
            try { Directory.Delete(PathForTest, true); }
            catch { }
            btOk.Enabled = false;
            SetHomeWorkLab(cbNumlab);
            cbNumlab.Enabled = false;
            cbNumVar.Enabled = false;

        }

        private void CreatDirectoryAndWorkWithCMD()//Копируем все файлы из папки сдающего в нашу папку "Для тестирования". Заускаем CMD, создаем .exe
        {
            string fileName = "";
            string onlyNameFile = "";
            string nameCs = "";
            string sourcePath = pathToUserCs;
            string targetPath = PathForTest;

            //Копирование из директории студента в директорию для работы
            string sourceFile = Path.Combine(sourcePath, fileName);
            string destFile = Path.Combine(targetPath, fileName);
            // System.IO.File.Copy(sourceFile, destFile, true);

            if (Directory.Exists(sourcePath))
            {
                string[] files = Directory.GetFiles(sourcePath);

                foreach (string s in files)
                {

                    fileName = Path.GetFileName(s);
                    destFile = Path.Combine(targetPath, fileName);
                    string[] m = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);//Делим имя по частям, чтобы не задавать его статически
                    if (m[1] == "cs")    //Это необходимо при написании запроса к командной строке
                    {
                        onlyNameFile = m[0] + ".exe";
                        nameCs = fileName;
                    }
                    try { File.Copy(s, destFile, true); }
                    catch
                    { MessageBox.Show("Вероятно, у вас открыт файл INPUT"); }
                   
                }
            }
            else
            {
                Console.WriteLine("Вы ничего не скопировали в папку для сдачи текущей работы.");
            }

            //создание .exe файла из .cs
            // string command = $"cd C:\\Temp\\Tests\nC:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\csc.exe {nameCs}\n{onlyNameFile}";
            /// мой пк
            string command = "cd C:\\Temp\\Tests" + "\n"+ "C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\csc.exe " + nameCs + "\n" + onlyNameFile;
            /// аудиторный
           // string command = "cd C:\\temp\\Tests" + "\n" + "C:\\WINDOWS\\Microsoft.NET\\Framework\\v4.0.30319\\csc.exe " + nameCs + "\n" + onlyNameFile;



            //string cmd = "@echo off" + "\n" + "cd C:\\Temp\\Tests" + "\n" + " C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\csc.exe " + nameCs + "\n" +onlyNameFile + ".exe";
            //string command = @cmd; // для командной


            File.WriteAllText("name.bat", command);

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    //WorkingDirectory = "C:\\Temp\\Tests", не надо прописывать
                    //FileName = "cmd.exe", //для командной

                    FileName = "name.bat",
                    RedirectStandardInput = true,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    Verb = "runas",
                    WindowStyle = ProcessWindowStyle.Hidden,//Скрывает запускающееся окно cmd
                    RedirectStandardError = true,
                    RedirectStandardOutput = true
                }
            };
            process.Start();
            //using (StreamWriter pWriter = process.StandardInput) //для командной
            //{
            //    if (pWriter.BaseStream.CanWrite)
            //    {
            //        foreach (var line in command.Split('\n'))
            //            pWriter.WriteLine(line);
            //    }
            //}


            if (!process.WaitForExit(1000000000)) process.Kill();//Если процесс работает слишком долго, то мы его убиваем
            //process.WaitForExit();
            
            
            if (flaglab2)
                ExaminationOutputFileLab();//проверка ответов
            if (flagsrs2)
                ExaminationOutputFileSrs();

            //File.Delete("name.bat");
            //try { Directory.Delete(WayForTest, true); }
            //catch { } 
           //Удаляем дирекуторию Test
           // process.Close();
            //this.Close();
        }

        private void btOk_Click(object sender, EventArgs e)
        {
            if (flaglab2)
            {
                CreateDirectotiesAndInpitFileLab();
                CreatDirectoryAndWorkWithCMD();
            }
            if (flagsrs2)
            {
                CreateDirectotiesAndInpitFileSrs();
                CreatDirectoryAndWorkWithCMD();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

