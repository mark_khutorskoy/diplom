﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Programm
{
    public partial class ResultStudent : Form
    {
        int curUserId;
        List<string> works;
        List<string> labs;
        List<string> srs;
        List<string> tests;

        public ResultStudent(int _curUserId)
        {
            InitializeComponent();
            dgvResults.AutoResizeColumns();
            curUserId = _curUserId;
            GetResults();
        }

        void GetResults()
        {            
            //Наполнение таблицы для студентов
                dgvResults.ColumnCount = 4;
                dgvResults.Columns[0].Name = "Раздел";
                dgvResults.Columns[1].Name = "Лаб";
                dgvResults.Columns[2].Name = "СРС";
                dgvResults.Columns[3].Name = "Тест";

                GetUserResults();

                for (int i = 0; i < works.Count(); i++)
                {
                    dgvResults.Rows.Add(works[i], labs[i], srs[i], tests[i]);
                }
        }

        //считываем из БД данные
        public void GetUserResults()
        {
            works = new List<string>();
            labs = new List<string>();
            srs = new List<string>();
            tests = new List<string>();
            using (var dataBase = new MyDatabase())
            {
                string queryGetWorks = @"select Name_Work, Res_Lab, Res_Srs, Res_Test from Res_Work where id_Aut = @id_Aut";
                SQLiteCommand commandqueryGetWorks = new SQLiteCommand(queryGetWorks, dataBase.sConnect);
                commandqueryGetWorks.Parameters.Add(new SQLiteParameter("id_Aut", curUserId));
                SQLiteDataReader reader = commandqueryGetWorks.ExecuteReader();
                while (reader.Read())
                {
                    if(reader["Name_Work"] != DBNull.Value)
                        works.Add((string)reader["Name_Work"]);
                    else works.Add(" ");

                    if (reader["Res_Lab"] != DBNull.Value)
                        labs.Add((string)reader["Res_Lab"]);
                    else labs.Add(" ");

                    if (reader["Res_Srs"] != DBNull.Value)
                        srs.Add((string)reader["Res_Srs"]);
                    else srs.Add(" ");

                    if (reader["Res_Test"] != DBNull.Value)
                        tests.Add((string)reader["Res_Test"]);
                    else tests.Add(" ");
                }
            }
        }

        private void ResultStudent_Load(object sender, EventArgs e)
        {
        }
    }
}
