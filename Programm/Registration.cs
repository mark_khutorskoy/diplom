﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
using System.Text.RegularExpressions;

namespace Programm
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
            InitGroup(cbGroup);
        }

        public void InitGroup(ComboBox x) //вывод списка групп
        {
            //Сделать отдельную таблицу!!! Для добавления новых групп.
            using (var dataBase = new MyDatabase())
            {
                string queryCount = @"select distinct [Group] from Authentication";
                SQLiteCommand commandCount = new SQLiteCommand(queryCount, dataBase.sConnect);
                var Reader = commandCount.ExecuteReader();
                x.Items.Clear();
                while (Reader.Read())
                {
                    if (Reader["Group"].ToString() != "admin")
                        x.Items.Add(Reader["Group"]);
                }
            }
        }

        public string fname
        {
            get { return tbFirstName.Text; }
            set { tbFirstName.Text = value; }
        }
        public string lname
        {
            get { return tbLastName.Text; }
            set { tbLastName.Text = value; }
        }
        public string pname
        {
            get { return tbPatronymicName.Text; }
            set { tbPatronymicName.Text = value; }
        }
        public string pas
        {
            get { return tbPassword.Text; }
            set { tbPassword.Text = value; }
        }
        public string group
        {
            get { return cbGroup.Text; }
            set { cbGroup.Text = value; }
        }

        public static string ColPerson; //???????

        public void Registr()
        {
            using (var dataBase = new MyDatabase())
            {
                var rnd = new Random();
                var salt = PassHash.CreateSalt(rnd.Next(20, 30));
                var password = PassHash.CreateHash(pas + salt);

                if ((tbPassword.Text == "") || tbLastName.Text == "" || tbFirstName.Text == "" || tbPatronymicName.Text == "" || cbGroup.Text == "")
                {
                    MessageBox.Show("Заполните все поля");
                }
                else if (tbPassword.Text != tbPassword2.Text)
                {
                    MessageBox.Show("Пароли не совпадают");
                    tbPassword.Clear();
                    tbPassword2.Clear();
                }
                //else if (IsNumberContains(tbLastName.Text) == false)
                //{
                //    MessageBox.Show("Для ввода используйте только русские буквы");
                //}
                else
                {
                    //добавляем пользователя к юзерам
                    string CommandText = @" INSERT INTO Authentication (First_Name,Last_Name,Patronymic_name,[Group],Password,salt,idRole)   VALUES  (@First_Name,@Last_Name,@Patronymic_Name,@Group,@Password,@salt,@idRole)";
                    SQLiteCommand sCommand = new SQLiteCommand(CommandText, dataBase.sConnect);
                    sCommand.Parameters.AddWithValue("First_Name", fname);
                    sCommand.Parameters.AddWithValue("Last_Name", lname);
                    sCommand.Parameters.AddWithValue("Patronymic_name", pname);
                    sCommand.Parameters.AddWithValue("Group", group);
                    sCommand.Parameters.AddWithValue("Password", password);
                    sCommand.Parameters.AddWithValue("salt", salt);
                    sCommand.Parameters.AddWithValue("idRole", "1"); // пока что добавляем только студента
                    sCommand.ExecuteNonQuery();

                    Authentication AuthenticationForm = new Authentication();
                    MessageBox.Show("Вы успешно зарегестированы");
                    this.Hide();
                    AuthenticationForm.Show();
                }
            }
        }
        //static bool IsNumberContains(string input)
        //{
        //    int i = 0;
        //    foreach (char c in input)
        //        if (Char.IsLetter(c) || Char.IsNumber(c) || Char.IsSymbol(c) ||Char.IsWhiteSpace(c))
        //            i++;
        //    if (i == 0) return true;
        //    else return false;
        //}
        private void btnOK_Click(object sender, EventArgs e)
        {
            Registr();
        }

        //Для ввода только русских букв в поля ФИО
        private void InputCheck(object sender, KeyPressEventArgs e)
        {
            if (!Regex.IsMatch(e.KeyChar.ToString(), "[А-Яа-я]") && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        //сделать всем соль и хэш в БД
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    List<string> temp = new List<string>();
            
        //    using (var dataBase = new DataBaseWork())
        //    {
        //        string CommandText = @"SELECT Password from Authentication";
        //        SQLiteCommand sCommand = new SQLiteCommand(CommandText, dataBase.sCon);
        //        var Reader = sCommand.ExecuteReader();
        //        while (Reader.Read())
        //        {
        //            temp.Add(Convert.ToString((Reader["Password"])));
        //        }

              
        //        for (int i = 0; i < temp.Count; i++)
        //        {
        //            var rnd = new Random();
        //            var salt = PassHash.CreateSalt(rnd.Next(20, 30));
        //            var password = PassHash.CreateHash(temp[i] + salt);

        //            string CommandText2 = @"UPDATE Authentication SET Password =:psd, salt=:salt WHERE id=:id";

        //            SQLiteCommand sCommand2 = new SQLiteCommand(CommandText2, dataBase.sCon);
        //            sCommand2.Parameters.AddWithValue("psd", password);
        //            sCommand2.Parameters.AddWithValue("salt", salt);
        //            sCommand2.Parameters.AddWithValue("id", i+1);
        //            sCommand2.ExecuteNonQuery();
        //        }

        //    }
        //}
    }
}