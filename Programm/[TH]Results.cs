﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Programm
{
    public partial class _TH_Results : Form
    {
        string group;
        List<string> lastnames = new List<string>();
        List<string> firstnames = new List<string>();
        List<string> patnames = new List<string>();
        List<string> works = new List<string>();
        List<string> labs = new List<string>();
        List<string> srs = new List<string>();
        List<string> tests = new List<string>();

        public _TH_Results(string _group)
        {
            InitializeComponent();
            group = _group;

            GetResults();
        }

        private void GetResults()
        {
            dgvResults.ColumnCount = 8;
            dgvResults.Columns[0].Name = "Фамилия";
            dgvResults.Columns[1].Name = "Имя";
            dgvResults.Columns[2].Name = "Отчество";
            dgvResults.Columns[3].Name = "Группа";
            dgvResults.Columns[4].Name = "Раздел";
            dgvResults.Columns[5].Name = "Лаб";
            dgvResults.Columns[6].Name = "СРС";
            dgvResults.Columns[7].Name = "Тест";

            int counter = 0; //счетчик количесвтва записей

            using (var dataBase = new MyDatabase())
            {
                string queryGetAll = @"SELECT First_Name, Last_Name, Patronymic_Name, Name_Work, Res_Lab, Res_Srs, Res_Test 
                                            FROM Authentication, Res_Work WHERE Authentication.id = Res_Work.id_Aut and Authentication.[Group] =@Group";
                SQLiteCommand commandqueryGetAll = new SQLiteCommand(queryGetAll, dataBase.sConnect);
                commandqueryGetAll.Parameters.Add(new SQLiteParameter("Group", group));
                SQLiteDataReader reader = commandqueryGetAll.ExecuteReader();
                while (reader.Read())
                {
                    counter++;

                    firstnames.Add((string)reader["First_Name"]);
                    lastnames.Add((string)reader["Last_Name"]);
                    patnames.Add((string)reader["Patronymic_Name"]);

                    if (reader["Name_Work"] != DBNull.Value)
                        works.Add((string)reader["Name_Work"]);
                    else works.Add(" ");

                    if (reader["Res_Lab"] != DBNull.Value)
                        labs.Add((string)reader["Res_Lab"]);
                    else labs.Add(" ");

                    if (reader["Res_Srs"] != DBNull.Value)
                        srs.Add((string)reader["Res_Srs"]);
                    else srs.Add(" ");

                    if (reader["Res_Test"] != DBNull.Value)
                        tests.Add((string)reader["Res_Test"]);
                    else tests.Add(" ");
                }
            }

            for (int i = 0; i < counter ; i++)
            {
                dgvResults.Rows.Add(lastnames[i], firstnames[i], patnames[i], group, works[i], labs[i], srs[i], tests[i]);
            }
        }

        private void toExcel_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon", "Oooops", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void groupselect_Click(object sender, EventArgs e)
        {
            SelectGroupForResultDGV groupSelect = new SelectGroupForResultDGV();
            groupSelect.Show();
        }

    }
}
