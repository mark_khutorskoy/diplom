﻿namespace Programm
{
    partial class TestTasks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbNumlab = new System.Windows.Forms.ComboBox();
            this.cbNumVar = new System.Windows.Forms.ComboBox();
            this.btRepozit = new System.Windows.Forms.Button();
            this.btOk = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbNumlab
            // 
            this.cbNumlab.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNumlab.FormattingEnabled = true;
            this.cbNumlab.Location = new System.Drawing.Point(0, 24);
            this.cbNumlab.Name = "cbNumlab";
            this.cbNumlab.Size = new System.Drawing.Size(206, 24);
            this.cbNumlab.TabIndex = 2;
            this.cbNumlab.SelectedIndexChanged += new System.EventHandler(this.cbNumlab_SelectedIndexChanged);
            // 
            // cbNumVar
            // 
            this.cbNumVar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNumVar.FormattingEnabled = true;
            this.cbNumVar.Location = new System.Drawing.Point(0, 38);
            this.cbNumVar.Name = "cbNumVar";
            this.cbNumVar.Size = new System.Drawing.Size(206, 24);
            this.cbNumVar.TabIndex = 3;
            // 
            // btRepozit
            // 
            this.btRepozit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btRepozit.Location = new System.Drawing.Point(68, 33);
            this.btRepozit.Name = "btRepozit";
            this.btRepozit.Size = new System.Drawing.Size(206, 47);
            this.btRepozit.TabIndex = 1;
            this.btRepozit.Text = "Указать путь к проверяемому файлу";
            this.btRepozit.UseVisualStyleBackColor = true;
            this.btRepozit.Click += new System.EventHandler(this.btRepozit_Click);
            // 
            // btOk
            // 
            this.btOk.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btOk.Location = new System.Drawing.Point(68, 270);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(206, 34);
            this.btOk.TabIndex = 4;
            this.btOk.Text = "Сдать работу";
            this.btOk.UseVisualStyleBackColor = false;
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbNumlab);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(68, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(206, 51);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выберите  раздел";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbNumVar);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(68, 178);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(206, 77);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Выберите лабораторную и вариант";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(68, 325);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(206, 30);
            this.button1.TabIndex = 5;
            this.button1.Text = "Назад";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TestTasks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 396);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btOk);
            this.Controls.Add(this.btRepozit);
            this.Name = "TestTasks";
            this.Text = "Сдача Работы";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbNumlab;
        private System.Windows.Forms.ComboBox cbNumVar;
        private System.Windows.Forms.Button btRepozit;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
    }
}