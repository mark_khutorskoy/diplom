﻿namespace Programm
{
    partial class _TH_DataBaseRevise
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_TH_DataBaseRevise));
            this.tpDel = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pbDeleteWork = new System.Windows.Forms.PictureBox();
            this.pbDeleteVar = new System.Windows.Forms.PictureBox();
            this.pbDeleteLabSRS = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbIdLabSRSDelete = new System.Windows.Forms.ComboBox();
            this.cbTypeDelete = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbVarDelete = new System.Windows.Forms.ComboBox();
            this.cbWorkDelete = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tbTextDelete = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.pbDeleteExaminationData = new System.Windows.Forms.PictureBox();
            this.btnRightDelete = new System.Windows.Forms.Button();
            this.btnLeftDelete = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbOutputDataDelete = new System.Windows.Forms.TextBox();
            this.tbInputDataDelete = new System.Windows.Forms.TextBox();
            this.tpAdd = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.LblType = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbCraftText = new System.Windows.Forms.PictureBox();
            this.lblVar = new System.Windows.Forms.Label();
            this.lblTaskNum = new System.Windows.Forms.Label();
            this.cbVariant = new System.Windows.Forms.ComboBox();
            this.cbWork = new System.Windows.Forms.ComboBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbTaskText = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblOutcoming = new System.Windows.Forms.Label();
            this.lblIncoming = new System.Windows.Forms.Label();
            this.tbOutputData = new System.Windows.Forms.TextBox();
            this.tbInputData = new System.Windows.Forms.TextBox();
            this.tbRevise = new System.Windows.Forms.TabControl();
            this.tpEdit = new System.Windows.Forms.TabPage();
            this.btnEdit = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbIdLabSRS = new System.Windows.Forms.ComboBox();
            this.cbTypeEdit = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbVarEdit = new System.Windows.Forms.ComboBox();
            this.cbWorkEdit = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tbTextEdit = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.pbEditTestData = new System.Windows.Forms.PictureBox();
            this.btnRight = new System.Windows.Forms.Button();
            this.pbAddTestData = new System.Windows.Forms.PictureBox();
            this.btnLeft = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbOutputDataEdit = new System.Windows.Forms.TextBox();
            this.tbInputDataEdit = new System.Windows.Forms.TextBox();
            this.tpHover = new System.Windows.Forms.ToolTip(this.components);
            this.tpDel.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteVar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteLabSRS)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteExaminationData)).BeginInit();
            this.tpAdd.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCraftText)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tbRevise.SuspendLayout();
            this.tpEdit.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEditTestData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddTestData)).BeginInit();
            this.SuspendLayout();
            // 
            // tpDel
            // 
            this.tpDel.Controls.Add(this.groupBox5);
            this.tpDel.Controls.Add(this.groupBox6);
            this.tpDel.Controls.Add(this.groupBox7);
            this.tpDel.Location = new System.Drawing.Point(4, 25);
            this.tpDel.Name = "tpDel";
            this.tpDel.Padding = new System.Windows.Forms.Padding(3);
            this.tpDel.Size = new System.Drawing.Size(630, 572);
            this.tpDel.TabIndex = 1;
            this.tpDel.Text = "Удалить";
            this.tpDel.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pbDeleteWork);
            this.groupBox5.Controls.Add(this.pbDeleteVar);
            this.groupBox5.Controls.Add(this.pbDeleteLabSRS);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.cbIdLabSRSDelete);
            this.groupBox5.Controls.Add(this.cbTypeDelete);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.cbVarDelete);
            this.groupBox5.Controls.Add(this.cbWorkDelete);
            this.groupBox5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox5.Location = new System.Drawing.Point(33, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(419, 158);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            // 
            // pbDeleteWork
            // 
            this.pbDeleteWork.Image = global::Programm.Properties.Resources.delete_icon;
            this.pbDeleteWork.Location = new System.Drawing.Point(384, 21);
            this.pbDeleteWork.Name = "pbDeleteWork";
            this.pbDeleteWork.Size = new System.Drawing.Size(29, 23);
            this.pbDeleteWork.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDeleteWork.TabIndex = 28;
            this.pbDeleteWork.TabStop = false;
            this.tpHover.SetToolTip(this.pbDeleteWork, "Удалить раздел и все, что внутри него");
            this.pbDeleteWork.Click += new System.EventHandler(this.pbDeleteWork_Click);
            // 
            // pbDeleteVar
            // 
            this.pbDeleteVar.Image = global::Programm.Properties.Resources.delete_icon;
            this.pbDeleteVar.Location = new System.Drawing.Point(384, 52);
            this.pbDeleteVar.Name = "pbDeleteVar";
            this.pbDeleteVar.Size = new System.Drawing.Size(29, 23);
            this.pbDeleteVar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDeleteVar.TabIndex = 27;
            this.pbDeleteVar.TabStop = false;
            this.tpHover.SetToolTip(this.pbDeleteVar, "Удалить тему и все, что внутри нее");
            this.pbDeleteVar.Click += new System.EventHandler(this.pbDeleteVar_Click);
            // 
            // pbDeleteLabSRS
            // 
            this.pbDeleteLabSRS.Image = global::Programm.Properties.Resources.delete_icon;
            this.pbDeleteLabSRS.Location = new System.Drawing.Point(384, 111);
            this.pbDeleteLabSRS.Name = "pbDeleteLabSRS";
            this.pbDeleteLabSRS.Size = new System.Drawing.Size(29, 23);
            this.pbDeleteLabSRS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDeleteLabSRS.TabIndex = 27;
            this.pbDeleteLabSRS.TabStop = false;
            this.tpHover.SetToolTip(this.pbDeleteLabSRS, "Удалить конкретную аудиторную работу или СРС");
            this.pbDeleteLabSRS.Click += new System.EventHandler(this.pbDeleteLabSRS_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(11, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 20);
            this.label7.TabIndex = 23;
            this.label7.Text = "Номер работы:";
            // 
            // cbIdLabSRSDelete
            // 
            this.cbIdLabSRSDelete.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIdLabSRSDelete.FormattingEnabled = true;
            this.cbIdLabSRSDelete.Location = new System.Drawing.Point(171, 108);
            this.cbIdLabSRSDelete.Name = "cbIdLabSRSDelete";
            this.cbIdLabSRSDelete.Size = new System.Drawing.Size(193, 24);
            this.cbIdLabSRSDelete.TabIndex = 22;
            this.cbIdLabSRSDelete.SelectedIndexChanged += new System.EventHandler(this.cbIdLabSRSDelete_SelectedIndexChanged);
            // 
            // cbTypeDelete
            // 
            this.cbTypeDelete.BackColor = System.Drawing.SystemColors.Window;
            this.cbTypeDelete.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeDelete.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cbTypeDelete.FormattingEnabled = true;
            this.cbTypeDelete.Items.AddRange(new object[] {
            "Аудиторная",
            "СРС"});
            this.cbTypeDelete.Location = new System.Drawing.Point(171, 78);
            this.cbTypeDelete.Name = "cbTypeDelete";
            this.cbTypeDelete.Size = new System.Drawing.Size(193, 24);
            this.cbTypeDelete.TabIndex = 1;
            this.cbTypeDelete.SelectedIndexChanged += new System.EventHandler(this.cbVarDelete_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(10, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Тип работы:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(10, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 20);
            this.label9.TabIndex = 9;
            this.label9.Text = "Тема:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(10, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 20);
            this.label10.TabIndex = 8;
            this.label10.Text = "Раздел: ";
            // 
            // cbVarDelete
            // 
            this.cbVarDelete.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVarDelete.FormattingEnabled = true;
            this.cbVarDelete.Location = new System.Drawing.Point(171, 48);
            this.cbVarDelete.Name = "cbVarDelete";
            this.cbVarDelete.Size = new System.Drawing.Size(193, 24);
            this.cbVarDelete.TabIndex = 7;
            this.cbVarDelete.SelectedIndexChanged += new System.EventHandler(this.cbVarDelete_SelectedIndexChanged);
            // 
            // cbWorkDelete
            // 
            this.cbWorkDelete.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkDelete.FormattingEnabled = true;
            this.cbWorkDelete.Location = new System.Drawing.Point(171, 18);
            this.cbWorkDelete.Name = "cbWorkDelete";
            this.cbWorkDelete.Size = new System.Drawing.Size(193, 24);
            this.cbWorkDelete.TabIndex = 6;
            this.cbWorkDelete.SelectedIndexChanged += new System.EventHandler(this.cbWorkDelete_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tbTextDelete);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox6.Location = new System.Drawing.Point(33, 300);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(576, 223);
            this.groupBox6.TabIndex = 24;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Текст задания";
            // 
            // tbTextDelete
            // 
            this.tbTextDelete.Location = new System.Drawing.Point(6, 25);
            this.tbTextDelete.Multiline = true;
            this.tbTextDelete.Name = "tbTextDelete";
            this.tbTextDelete.ReadOnly = true;
            this.tbTextDelete.Size = new System.Drawing.Size(564, 190);
            this.tbTextDelete.TabIndex = 12;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.pbDeleteExaminationData);
            this.groupBox7.Controls.Add(this.btnRightDelete);
            this.groupBox7.Controls.Add(this.btnLeftDelete);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Controls.Add(this.tbOutputDataDelete);
            this.groupBox7.Controls.Add(this.tbInputDataDelete);
            this.groupBox7.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox7.Location = new System.Drawing.Point(33, 182);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(419, 100);
            this.groupBox7.TabIndex = 23;
            this.groupBox7.TabStop = false;
            // 
            // pbDeleteExaminationData
            // 
            this.pbDeleteExaminationData.Image = global::Programm.Properties.Resources.delete_icon;
            this.pbDeleteExaminationData.Location = new System.Drawing.Point(334, 46);
            this.pbDeleteExaminationData.Name = "pbDeleteExaminationData";
            this.pbDeleteExaminationData.Size = new System.Drawing.Size(29, 23);
            this.pbDeleteExaminationData.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDeleteExaminationData.TabIndex = 26;
            this.pbDeleteExaminationData.TabStop = false;
            this.tpHover.SetToolTip(this.pbDeleteExaminationData, "Удалить набор данных");
            this.pbDeleteExaminationData.Click += new System.EventHandler(this.pbDeleteExaminationData_Click);
            // 
            // btnRightDelete
            // 
            this.btnRightDelete.Location = new System.Drawing.Point(282, 75);
            this.btnRightDelete.Name = "btnRightDelete";
            this.btnRightDelete.Size = new System.Drawing.Size(32, 23);
            this.btnRightDelete.TabIndex = 25;
            this.btnRightDelete.Text = "->";
            this.btnRightDelete.UseVisualStyleBackColor = true;
            this.btnRightDelete.Click += new System.EventHandler(this.btnRightDelete_Click);
            // 
            // btnLeftDelete
            // 
            this.btnLeftDelete.Location = new System.Drawing.Point(193, 75);
            this.btnLeftDelete.Name = "btnLeftDelete";
            this.btnLeftDelete.Size = new System.Drawing.Size(32, 23);
            this.btnLeftDelete.TabIndex = 24;
            this.btnLeftDelete.Text = "<-";
            this.btnLeftDelete.UseVisualStyleBackColor = true;
            this.btnLeftDelete.Click += new System.EventHandler(this.btnLeftDelete_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(10, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(169, 20);
            this.label11.TabIndex = 11;
            this.label11.Text = "Выходные данные:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(10, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(157, 20);
            this.label12.TabIndex = 10;
            this.label12.Text = "Входные данные:";
            // 
            // tbOutputDataDelete
            // 
            this.tbOutputDataDelete.Location = new System.Drawing.Point(193, 47);
            this.tbOutputDataDelete.Name = "tbOutputDataDelete";
            this.tbOutputDataDelete.ReadOnly = true;
            this.tbOutputDataDelete.Size = new System.Drawing.Size(121, 22);
            this.tbOutputDataDelete.TabIndex = 9;
            // 
            // tbInputDataDelete
            // 
            this.tbInputDataDelete.Location = new System.Drawing.Point(193, 19);
            this.tbInputDataDelete.Name = "tbInputDataDelete";
            this.tbInputDataDelete.ReadOnly = true;
            this.tbInputDataDelete.Size = new System.Drawing.Size(121, 22);
            this.tbInputDataDelete.TabIndex = 8;
            // 
            // tpAdd
            // 
            this.tpAdd.Controls.Add(this.label13);
            this.tpAdd.Controls.Add(this.groupBox4);
            this.tpAdd.Controls.Add(this.groupBox1);
            this.tpAdd.Controls.Add(this.btnAdd);
            this.tpAdd.Controls.Add(this.groupBox3);
            this.tpAdd.Controls.Add(this.groupBox2);
            this.tpAdd.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tpAdd.Location = new System.Drawing.Point(4, 25);
            this.tpAdd.Name = "tpAdd";
            this.tpAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tpAdd.Size = new System.Drawing.Size(630, 572);
            this.tpAdd.TabIndex = 0;
            this.tpAdd.Text = "Добавить";
            this.tpAdd.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(40, 98);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(465, 17);
            this.label13.TabIndex = 17;
            this.label13.Text = "Создавайте новую тему в точности по шаблону: название;вариант N";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbType);
            this.groupBox4.Controls.Add(this.LblType);
            this.groupBox4.Location = new System.Drawing.Point(33, 122);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(353, 54);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "Аудиторная",
            "СРС"});
            this.cbType.Location = new System.Drawing.Point(193, 14);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(143, 24);
            this.cbType.TabIndex = 1;
            // 
            // LblType
            // 
            this.LblType.AutoSize = true;
            this.LblType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblType.Location = new System.Drawing.Point(10, 18);
            this.LblType.Name = "LblType";
            this.LblType.Size = new System.Drawing.Size(111, 20);
            this.LblType.TabIndex = 0;
            this.LblType.Text = "Тип работы:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbCraftText);
            this.groupBox1.Controls.Add(this.lblVar);
            this.groupBox1.Controls.Add(this.lblTaskNum);
            this.groupBox1.Controls.Add(this.cbVariant);
            this.groupBox1.Controls.Add(this.cbWork);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox1.Location = new System.Drawing.Point(33, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 85);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // pbCraftText
            // 
            this.pbCraftText.Image = global::Programm.Properties.Resources.Arrow_Down;
            this.pbCraftText.Location = new System.Drawing.Point(370, 34);
            this.pbCraftText.Name = "pbCraftText";
            this.pbCraftText.Size = new System.Drawing.Size(29, 23);
            this.pbCraftText.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCraftText.TabIndex = 18;
            this.pbCraftText.TabStop = false;
            this.tpHover.SetToolTip(this.pbCraftText, "Генерирует \"Текст задания\". Нажмите, если создаете новый раздел или тему. \r\nДля с" +
        "уществующих текст задания генерируется автоматически после выбора элементов в по" +
        "лях раздел и тема.");
            this.pbCraftText.Click += new System.EventHandler(this.pbCraftText_Click);
            // 
            // lblVar
            // 
            this.lblVar.AutoSize = true;
            this.lblVar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVar.Location = new System.Drawing.Point(10, 52);
            this.lblVar.Name = "lblVar";
            this.lblVar.Size = new System.Drawing.Size(56, 20);
            this.lblVar.TabIndex = 9;
            this.lblVar.Text = "Тема:";
            // 
            // lblTaskNum
            // 
            this.lblTaskNum.AutoSize = true;
            this.lblTaskNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTaskNum.Location = new System.Drawing.Point(10, 18);
            this.lblTaskNum.Name = "lblTaskNum";
            this.lblTaskNum.Size = new System.Drawing.Size(81, 20);
            this.lblTaskNum.TabIndex = 8;
            this.lblTaskNum.Text = "Раздел: ";
            // 
            // cbVariant
            // 
            this.cbVariant.FormattingEnabled = true;
            this.cbVariant.Location = new System.Drawing.Point(171, 48);
            this.cbVariant.Name = "cbVariant";
            this.cbVariant.Size = new System.Drawing.Size(193, 24);
            this.cbVariant.TabIndex = 7;
            this.cbVariant.SelectedIndexChanged += new System.EventHandler(this.cbVariant_SelectedIndexChanged);
            // 
            // cbWork
            // 
            this.cbWork.FormattingEnabled = true;
            this.cbWork.Location = new System.Drawing.Point(171, 18);
            this.cbWork.Name = "cbWork";
            this.cbWork.Size = new System.Drawing.Size(193, 24);
            this.cbWork.TabIndex = 6;
            this.cbWork.SelectedIndexChanged += new System.EventHandler(this.cbLab_SelectedIndexChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAdd.Location = new System.Drawing.Point(250, 508);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(119, 35);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbTaskText);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(33, 280);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(576, 222);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Текст задания";
            // 
            // tbTaskText
            // 
            this.tbTaskText.Location = new System.Drawing.Point(6, 25);
            this.tbTaskText.Multiline = true;
            this.tbTaskText.Name = "tbTaskText";
            this.tbTaskText.Size = new System.Drawing.Size(564, 190);
            this.tbTaskText.TabIndex = 12;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblOutcoming);
            this.groupBox2.Controls.Add(this.lblIncoming);
            this.groupBox2.Controls.Add(this.tbOutputData);
            this.groupBox2.Controls.Add(this.tbInputData);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox2.Location = new System.Drawing.Point(33, 182);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(353, 80);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // lblOutcoming
            // 
            this.lblOutcoming.AutoSize = true;
            this.lblOutcoming.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblOutcoming.Location = new System.Drawing.Point(10, 47);
            this.lblOutcoming.Name = "lblOutcoming";
            this.lblOutcoming.Size = new System.Drawing.Size(169, 20);
            this.lblOutcoming.TabIndex = 11;
            this.lblOutcoming.Text = "Выходные данные:";
            // 
            // lblIncoming
            // 
            this.lblIncoming.AutoSize = true;
            this.lblIncoming.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIncoming.Location = new System.Drawing.Point(10, 18);
            this.lblIncoming.Name = "lblIncoming";
            this.lblIncoming.Size = new System.Drawing.Size(157, 20);
            this.lblIncoming.TabIndex = 10;
            this.lblIncoming.Text = "Входные данные:";
            // 
            // tbOutputData
            // 
            this.tbOutputData.Location = new System.Drawing.Point(193, 47);
            this.tbOutputData.Name = "tbOutputData";
            this.tbOutputData.Size = new System.Drawing.Size(121, 22);
            this.tbOutputData.TabIndex = 9;
            // 
            // tbInputData
            // 
            this.tbInputData.Location = new System.Drawing.Point(193, 19);
            this.tbInputData.Name = "tbInputData";
            this.tbInputData.Size = new System.Drawing.Size(121, 22);
            this.tbInputData.TabIndex = 8;
            // 
            // tbRevise
            // 
            this.tbRevise.Controls.Add(this.tpAdd);
            this.tbRevise.Controls.Add(this.tpEdit);
            this.tbRevise.Controls.Add(this.tpDel);
            this.tbRevise.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRevise.Location = new System.Drawing.Point(0, 0);
            this.tbRevise.Name = "tbRevise";
            this.tbRevise.SelectedIndex = 0;
            this.tbRevise.Size = new System.Drawing.Size(638, 601);
            this.tbRevise.TabIndex = 16;
            // 
            // tpEdit
            // 
            this.tpEdit.Controls.Add(this.btnEdit);
            this.tpEdit.Controls.Add(this.groupBox8);
            this.tpEdit.Controls.Add(this.groupBox9);
            this.tpEdit.Controls.Add(this.groupBox10);
            this.tpEdit.Location = new System.Drawing.Point(4, 25);
            this.tpEdit.Name = "tpEdit";
            this.tpEdit.Size = new System.Drawing.Size(630, 572);
            this.tpEdit.TabIndex = 2;
            this.tpEdit.Text = "Изменить";
            this.tpEdit.UseVisualStyleBackColor = true;
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEdit.Location = new System.Drawing.Point(249, 529);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(119, 35);
            this.btnEdit.TabIndex = 21;
            this.btnEdit.Text = "Изменить";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label4);
            this.groupBox8.Controls.Add(this.cbIdLabSRS);
            this.groupBox8.Controls.Add(this.cbTypeEdit);
            this.groupBox8.Controls.Add(this.label1);
            this.groupBox8.Controls.Add(this.label2);
            this.groupBox8.Controls.Add(this.label3);
            this.groupBox8.Controls.Add(this.cbVarEdit);
            this.groupBox8.Controls.Add(this.cbWorkEdit);
            this.groupBox8.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox8.Location = new System.Drawing.Point(33, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(384, 158);
            this.groupBox8.TabIndex = 17;
            this.groupBox8.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(11, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 20);
            this.label4.TabIndex = 23;
            this.label4.Text = "Номер работы:";
            // 
            // cbIdLabSRS
            // 
            this.cbIdLabSRS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIdLabSRS.FormattingEnabled = true;
            this.cbIdLabSRS.Location = new System.Drawing.Point(170, 108);
            this.cbIdLabSRS.Name = "cbIdLabSRS";
            this.cbIdLabSRS.Size = new System.Drawing.Size(193, 24);
            this.cbIdLabSRS.TabIndex = 22;
            this.cbIdLabSRS.SelectedIndexChanged += new System.EventHandler(this.cbIdLabSRS_SelectedIndexChanged);
            // 
            // cbTypeEdit
            // 
            this.cbTypeEdit.BackColor = System.Drawing.SystemColors.Window;
            this.cbTypeEdit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeEdit.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cbTypeEdit.FormattingEnabled = true;
            this.cbTypeEdit.Items.AddRange(new object[] {
            "Аудиторная",
            "СРС"});
            this.cbTypeEdit.Location = new System.Drawing.Point(170, 78);
            this.cbTypeEdit.Name = "cbTypeEdit";
            this.cbTypeEdit.Size = new System.Drawing.Size(193, 24);
            this.cbTypeEdit.TabIndex = 1;
            this.cbTypeEdit.SelectedIndexChanged += new System.EventHandler(this.cbTypeEdit_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(10, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Тип работы:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(10, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Тема:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(10, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Раздел: ";
            // 
            // cbVarEdit
            // 
            this.cbVarEdit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVarEdit.FormattingEnabled = true;
            this.cbVarEdit.Location = new System.Drawing.Point(170, 48);
            this.cbVarEdit.Name = "cbVarEdit";
            this.cbVarEdit.Size = new System.Drawing.Size(193, 24);
            this.cbVarEdit.TabIndex = 7;
            this.cbVarEdit.SelectedIndexChanged += new System.EventHandler(this.cbTypeEdit_SelectedIndexChanged);
            // 
            // cbWorkEdit
            // 
            this.cbWorkEdit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkEdit.FormattingEnabled = true;
            this.cbWorkEdit.Location = new System.Drawing.Point(170, 18);
            this.cbWorkEdit.Name = "cbWorkEdit";
            this.cbWorkEdit.Size = new System.Drawing.Size(193, 24);
            this.cbWorkEdit.TabIndex = 6;
            this.cbWorkEdit.SelectedIndexChanged += new System.EventHandler(this.cbWorkEdit_SelectedIndexChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.tbTextEdit);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox9.Location = new System.Drawing.Point(33, 300);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(576, 223);
            this.groupBox9.TabIndex = 19;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Текст задания";
            // 
            // tbTextEdit
            // 
            this.tbTextEdit.Location = new System.Drawing.Point(6, 25);
            this.tbTextEdit.Multiline = true;
            this.tbTextEdit.Name = "tbTextEdit";
            this.tbTextEdit.Size = new System.Drawing.Size(564, 190);
            this.tbTextEdit.TabIndex = 12;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.pbEditTestData);
            this.groupBox10.Controls.Add(this.btnRight);
            this.groupBox10.Controls.Add(this.pbAddTestData);
            this.groupBox10.Controls.Add(this.btnLeft);
            this.groupBox10.Controls.Add(this.label5);
            this.groupBox10.Controls.Add(this.label6);
            this.groupBox10.Controls.Add(this.tbOutputDataEdit);
            this.groupBox10.Controls.Add(this.tbInputDataEdit);
            this.groupBox10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox10.Location = new System.Drawing.Point(33, 182);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(384, 100);
            this.groupBox10.TabIndex = 18;
            this.groupBox10.TabStop = false;
            // 
            // pbEditTestData
            // 
            this.pbEditTestData.Image = global::Programm.Properties.Resources.edit;
            this.pbEditTestData.Location = new System.Drawing.Point(334, 46);
            this.pbEditTestData.Name = "pbEditTestData";
            this.pbEditTestData.Size = new System.Drawing.Size(29, 23);
            this.pbEditTestData.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbEditTestData.TabIndex = 26;
            this.pbEditTestData.TabStop = false;
            this.tpHover.SetToolTip(this.pbEditTestData, "Изменить набор данных");
            this.pbEditTestData.Click += new System.EventHandler(this.pbEditTestData_Click);
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(282, 75);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(32, 23);
            this.btnRight.TabIndex = 25;
            this.btnRight.Text = "->";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // pbAddTestData
            // 
            this.pbAddTestData.Image = ((System.Drawing.Image)(resources.GetObject("pbAddTestData.Image")));
            this.pbAddTestData.Location = new System.Drawing.Point(334, 18);
            this.pbAddTestData.Name = "pbAddTestData";
            this.pbAddTestData.Size = new System.Drawing.Size(29, 23);
            this.pbAddTestData.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAddTestData.TabIndex = 12;
            this.pbAddTestData.TabStop = false;
            this.tpHover.SetToolTip(this.pbAddTestData, "Добавить набор данных");
            this.pbAddTestData.Click += new System.EventHandler(this.pbAddTestData_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(193, 75);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(32, 23);
            this.btnLeft.TabIndex = 24;
            this.btnLeft.Text = "<-";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(10, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Выходные данные:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(10, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Входные данные:";
            // 
            // tbOutputDataEdit
            // 
            this.tbOutputDataEdit.Location = new System.Drawing.Point(193, 47);
            this.tbOutputDataEdit.Name = "tbOutputDataEdit";
            this.tbOutputDataEdit.Size = new System.Drawing.Size(121, 22);
            this.tbOutputDataEdit.TabIndex = 9;
            // 
            // tbInputDataEdit
            // 
            this.tbInputDataEdit.Location = new System.Drawing.Point(193, 19);
            this.tbInputDataEdit.Name = "tbInputDataEdit";
            this.tbInputDataEdit.Size = new System.Drawing.Size(121, 22);
            this.tbInputDataEdit.TabIndex = 8;
            // 
            // _TH_DataBaseRevise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(638, 601);
            this.Controls.Add(this.tbRevise);
            this.MaximizeBox = false;
            this.Name = "_TH_DataBaseRevise";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Редактор заданий";
            this.tpDel.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteVar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteLabSRS)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeleteExaminationData)).EndInit();
            this.tpAdd.ResumeLayout(false);
            this.tpAdd.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCraftText)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tbRevise.ResumeLayout(false);
            this.tpEdit.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEditTestData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddTestData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tpDel;
        private System.Windows.Forms.TabPage tpAdd;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label LblType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblVar;
        private System.Windows.Forms.Label lblTaskNum;
        private System.Windows.Forms.ComboBox cbVariant;
        private System.Windows.Forms.ComboBox cbWork;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbTaskText;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblOutcoming;
        private System.Windows.Forms.Label lblIncoming;
        private System.Windows.Forms.TextBox tbOutputData;
        private System.Windows.Forms.TextBox tbInputData;
        private System.Windows.Forms.TabControl tbRevise;
        private System.Windows.Forms.TabPage tpEdit;
        private System.Windows.Forms.ComboBox cbTypeEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbVarEdit;
        private System.Windows.Forms.ComboBox cbWorkEdit;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox tbTextEdit;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.PictureBox pbAddTestData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbOutputDataEdit;
        private System.Windows.Forms.TextBox tbInputDataEdit;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbIdLabSRS;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.PictureBox pbEditTestData;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbIdLabSRSDelete;
        private System.Windows.Forms.ComboBox cbTypeDelete;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbVarDelete;
        private System.Windows.Forms.ComboBox cbWorkDelete;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tbTextDelete;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btnRightDelete;
        private System.Windows.Forms.Button btnLeftDelete;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbOutputDataDelete;
        private System.Windows.Forms.TextBox tbInputDataDelete;
        private System.Windows.Forms.PictureBox pbDeleteExaminationData;
        private System.Windows.Forms.PictureBox pbDeleteWork;
        private System.Windows.Forms.PictureBox pbDeleteVar;
        private System.Windows.Forms.PictureBox pbDeleteLabSRS;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolTip tpHover;
        private System.Windows.Forms.PictureBox pbCraftText;
    }
}