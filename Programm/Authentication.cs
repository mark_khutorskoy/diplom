﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
using System.Text.RegularExpressions;

namespace Programm
{
    public partial class Authentication : Form
    {
        public Authentication()
        {
            InitializeComponent();
        }

        private void CheckInput(object sender, KeyPressEventArgs e)
        {
            if (!Regex.IsMatch(e.KeyChar.ToString(), "[А-Яа-яA-Za-z]") && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private bool Auth()
        {
            using (var db = new MyDatabase())
            {
                string query =
                    "select id, last_name, password, salt, idRole " +
                    "from Authentication where last_name = @LastName";
                SQLiteCommand command = new SQLiteCommand(query, db.sConnect);
                command.Parameters.Add(new SQLiteParameter("@LastName", txtLastName.Text));

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (PassHash.CheckHash(reader["password"].ToString(), txtPassword.Text + reader["salt"]))
                    {
                        Properties.Settings.Default.IsTeacher = reader["idRole"].ToString() == "2";
                        Properties.Settings.Default.UserId = Int32.Parse(reader["id"].ToString());
                        Properties.Settings.Default.Save();
                        return true;
                    }
                }
                return false;
            }
        }

        private void Auth_Click(object sender, EventArgs e)
        {
            if (Auth())
            {
                Visible = false;
                new MainWindow().ShowDialog();
                Visible = true;
            }
            else
            {
                MessageBox.Show("Проверьте правильность введенных данных");
            }
        }
    }
}