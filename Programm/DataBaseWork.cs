﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Programm
{
   public class MyDatabase :IDisposable
    {
        public SQLiteConnection sConnect { get; private set; }

        public MyDatabase()
        {
            StartConnection();
        }

        public void Dispose()// для удаления переменной dataBase в TV
        {
            sConnect.Close();
        }
        private bool StartConnection() //Подключение к БД
        {
            String StringConnection = @"Data Source = Resources/DataBase.sqlite;Version=3;";
            sConnect = new SQLiteConnection(StringConnection);
            try
            {
                sConnect.Open();
                return true;
            }
            catch
            {
                MessageBox.Show("Not opened");
                return false;
            }
        }

    }
}
