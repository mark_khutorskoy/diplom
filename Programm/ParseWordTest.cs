﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices.ComTypes;
using System.Reflection;
using System.IO;
using System.Windows.Forms;

namespace Programm
{
    class ParseWordTest
    {
        public string ShowWord(string fullpath)
        {
            //fullpath = fullpath + ".rtf";
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            object miss = System.Reflection.Missing.Value;
            // object path = @"теория и лабораторные\" + fullpath;
            //object absolutePath = Path.GetFullPath((string)path);
            //object absolutePath = @"C:\Users\MarkNew\Desktop\" + fullpath;
            object absolutePath = fullpath;
            object readOnly = true;

            try
            {
                Microsoft.Office.Interop.Word.Document docs = word.Documents.Open(ref absolutePath, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);
                string curParagraph = "";
                string zeroresult = "";

                for (int i = 0; i < docs.Paragraphs.Count; i++)
                {
                    if (i == 4) //если набрано 0 баллов, то нужная строчка пятая
                    {
                        zeroresult = docs.Paragraphs[i + 1].Range.Text.ToString();
                    }

                    if (i == 5) //если больше 0 балллов, то 6
                    {
                        curParagraph = docs.Paragraphs[i + 1].Range.Text.ToString();
                        string[] split = curParagraph.Split(' ');

                        int res; //нужна просто для парса
                        if (Int32.TryParse(split[0], out res)) //если число, то это нужная строка
                        {
                            return curParagraph;

                        }
                        else //иначе нужно взять следующую (5-ю)
                        {
                            return zeroresult;
                        }
                    }
                }
                docs.Close();
                word.Quit();
            }
            catch { }
            return " ";
        }
    }
}
