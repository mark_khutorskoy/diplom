﻿namespace Programm
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcMode = new System.Windows.Forms.TabControl();
            this.StudentMode = new System.Windows.Forms.TabPage();
            this.lblGroup = new System.Windows.Forms.Label();
            this.lblFIO = new System.Windows.Forms.Label();
            this.pbMainLogo = new System.Windows.Forms.PictureBox();
            this.btnDZLoad = new System.Windows.Forms.Button();
            this.btnOpenResults = new System.Windows.Forms.Button();
            this.btnOpenTV = new System.Windows.Forms.Button();
            this.TeacherMode = new System.Windows.Forms.TabPage();
            this.btnEdit = new System.Windows.Forms.Button();
            this.lblFIOTH = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnStuResults = new System.Windows.Forms.Button();
            this.btnTHOpenTV = new System.Windows.Forms.Button();
            this.tcMode.SuspendLayout();
            this.StudentMode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMainLogo)).BeginInit();
            this.TeacherMode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tcMode
            // 
            this.tcMode.Controls.Add(this.StudentMode);
            this.tcMode.Controls.Add(this.TeacherMode);
            this.tcMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMode.Location = new System.Drawing.Point(0, 0);
            this.tcMode.Name = "tcMode";
            this.tcMode.SelectedIndex = 0;
            this.tcMode.Size = new System.Drawing.Size(789, 376);
            this.tcMode.TabIndex = 16;
            // 
            // StudentMode
            // 
            this.StudentMode.Controls.Add(this.lblGroup);
            this.StudentMode.Controls.Add(this.lblFIO);
            this.StudentMode.Controls.Add(this.pbMainLogo);
            this.StudentMode.Controls.Add(this.btnDZLoad);
            this.StudentMode.Controls.Add(this.btnOpenResults);
            this.StudentMode.Controls.Add(this.btnOpenTV);
            this.StudentMode.Location = new System.Drawing.Point(4, 25);
            this.StudentMode.Name = "StudentMode";
            this.StudentMode.Padding = new System.Windows.Forms.Padding(3);
            this.StudentMode.Size = new System.Drawing.Size(781, 347);
            this.StudentMode.TabIndex = 0;
            this.StudentMode.UseVisualStyleBackColor = true;
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblGroup.ForeColor = System.Drawing.Color.DimGray;
            this.lblGroup.Location = new System.Drawing.Point(19, 302);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblGroup.Size = new System.Drawing.Size(67, 20);
            this.lblGroup.TabIndex = 20;
            this.lblGroup.Text = "Группа";
            // 
            // lblFIO
            // 
            this.lblFIO.AutoSize = true;
            this.lblFIO.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFIO.ForeColor = System.Drawing.Color.DimGray;
            this.lblFIO.Location = new System.Drawing.Point(19, 266);
            this.lblFIO.Name = "lblFIO";
            this.lblFIO.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblFIO.Size = new System.Drawing.Size(49, 20);
            this.lblFIO.TabIndex = 19;
            this.lblFIO.Text = "ФИО";
            // 
            // pbMainLogo
            // 
            this.pbMainLogo.Image = global::Programm.Properties.Resources.MainLogo;
            this.pbMainLogo.Location = new System.Drawing.Point(60, 8);
            this.pbMainLogo.Margin = new System.Windows.Forms.Padding(5);
            this.pbMainLogo.Name = "pbMainLogo";
            this.pbMainLogo.Size = new System.Drawing.Size(651, 147);
            this.pbMainLogo.TabIndex = 18;
            this.pbMainLogo.TabStop = false;
            // 
            // btnDZLoad
            // 
            this.btnDZLoad.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDZLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDZLoad.Location = new System.Drawing.Point(264, 174);
            this.btnDZLoad.Margin = new System.Windows.Forms.Padding(5);
            this.btnDZLoad.Name = "btnDZLoad";
            this.btnDZLoad.Size = new System.Drawing.Size(236, 74);
            this.btnDZLoad.TabIndex = 17;
            this.btnDZLoad.Text = "Сдать СРС на проверку";
            this.btnDZLoad.UseVisualStyleBackColor = false;
            this.btnDZLoad.Click += new System.EventHandler(this.btnDZLoad_Click);
            // 
            // btnOpenResults
            // 
            this.btnOpenResults.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnOpenResults.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnOpenResults.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnOpenResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOpenResults.Location = new System.Drawing.Point(510, 174);
            this.btnOpenResults.Margin = new System.Windows.Forms.Padding(5);
            this.btnOpenResults.Name = "btnOpenResults";
            this.btnOpenResults.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnOpenResults.Size = new System.Drawing.Size(236, 74);
            this.btnOpenResults.TabIndex = 16;
            this.btnOpenResults.Text = "Посмотреть свои результаты";
            this.btnOpenResults.UseVisualStyleBackColor = false;
            this.btnOpenResults.Click += new System.EventHandler(this.btnOpenResults_Click);
            // 
            // btnOpenTV
            // 
            this.btnOpenTV.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnOpenTV.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOpenTV.Location = new System.Drawing.Point(19, 174);
            this.btnOpenTV.Margin = new System.Windows.Forms.Padding(4);
            this.btnOpenTV.Name = "btnOpenTV";
            this.btnOpenTV.Size = new System.Drawing.Size(236, 74);
            this.btnOpenTV.TabIndex = 15;
            this.btnOpenTV.Text = "Перейти к обучению";
            this.btnOpenTV.UseVisualStyleBackColor = false;
            this.btnOpenTV.Click += new System.EventHandler(this.btnOpenTV_Click);
            // 
            // TeacherMode
            // 
            this.TeacherMode.Controls.Add(this.btnEdit);
            this.TeacherMode.Controls.Add(this.lblFIOTH);
            this.TeacherMode.Controls.Add(this.pictureBox1);
            this.TeacherMode.Controls.Add(this.btnStuResults);
            this.TeacherMode.Controls.Add(this.btnTHOpenTV);
            this.TeacherMode.Location = new System.Drawing.Point(4, 25);
            this.TeacherMode.Name = "TeacherMode";
            this.TeacherMode.Padding = new System.Windows.Forms.Padding(3);
            this.TeacherMode.Size = new System.Drawing.Size(781, 347);
            this.TeacherMode.TabIndex = 1;
            this.TeacherMode.UseVisualStyleBackColor = true;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEdit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEdit.Location = new System.Drawing.Point(264, 174);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnEdit.Size = new System.Drawing.Size(236, 74);
            this.btnEdit.TabIndex = 27;
            this.btnEdit.Text = "Редактировать задания";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // lblFIOTH
            // 
            this.lblFIOTH.AutoSize = true;
            this.lblFIOTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFIOTH.ForeColor = System.Drawing.Color.DimGray;
            this.lblFIOTH.Location = new System.Drawing.Point(19, 266);
            this.lblFIOTH.Name = "lblFIOTH";
            this.lblFIOTH.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblFIOTH.Size = new System.Drawing.Size(49, 20);
            this.lblFIOTH.TabIndex = 25;
            this.lblFIOTH.Text = "ФИО";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Programm.Properties.Resources.MainLogo;
            this.pictureBox1.Location = new System.Drawing.Point(60, 8);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(651, 147);
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // btnStuResults
            // 
            this.btnStuResults.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnStuResults.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnStuResults.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnStuResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnStuResults.Location = new System.Drawing.Point(510, 174);
            this.btnStuResults.Margin = new System.Windows.Forms.Padding(5);
            this.btnStuResults.Name = "btnStuResults";
            this.btnStuResults.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnStuResults.Size = new System.Drawing.Size(236, 74);
            this.btnStuResults.TabIndex = 22;
            this.btnStuResults.Text = "Результаты студентов";
            this.btnStuResults.UseVisualStyleBackColor = false;
            this.btnStuResults.Click += new System.EventHandler(this.btnStuResults_Click);
            // 
            // btnTHOpenTV
            // 
            this.btnTHOpenTV.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTHOpenTV.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnTHOpenTV.Location = new System.Drawing.Point(19, 174);
            this.btnTHOpenTV.Margin = new System.Windows.Forms.Padding(4);
            this.btnTHOpenTV.Name = "btnTHOpenTV";
            this.btnTHOpenTV.Size = new System.Drawing.Size(236, 74);
            this.btnTHOpenTV.TabIndex = 21;
            this.btnTHOpenTV.Text = "Перейти к обучению";
            this.btnTHOpenTV.UseVisualStyleBackColor = false;
            this.btnTHOpenTV.Click += new System.EventHandler(this.btnTHOpenTV_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(789, 376);
            this.Controls.Add(this.tcMode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Обучающий ресурс";
            this.tcMode.ResumeLayout(false);
            this.StudentMode.ResumeLayout(false);
            this.StudentMode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMainLogo)).EndInit();
            this.TeacherMode.ResumeLayout(false);
            this.TeacherMode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMode;
        private System.Windows.Forms.TabPage StudentMode;
        private System.Windows.Forms.TabPage TeacherMode;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label lblFIO;
        private System.Windows.Forms.PictureBox pbMainLogo;
        private System.Windows.Forms.Button btnDZLoad;
        private System.Windows.Forms.Button btnOpenResults;
        private System.Windows.Forms.Button btnOpenTV;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label lblFIOTH;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnStuResults;
        private System.Windows.Forms.Button btnTHOpenTV;
    }
}