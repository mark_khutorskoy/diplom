﻿namespace Programm
{
    partial class ForTestEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblinstruction = new System.Windows.Forms.Label();
            this.tbTestName = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.nudTestNum = new System.Windows.Forms.NumericUpDown();
            this.lblTestNum = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudTestNum)).BeginInit();
            this.SuspendLayout();
            // 
            // lblinstruction
            // 
            this.lblinstruction.AutoSize = true;
            this.lblinstruction.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblinstruction.Location = new System.Drawing.Point(12, 50);
            this.lblinstruction.Name = "lblinstruction";
            this.lblinstruction.Size = new System.Drawing.Size(226, 20);
            this.lblinstruction.TabIndex = 0;
            this.lblinstruction.Text = "Введите название теста:";
            // 
            // tbTestName
            // 
            this.tbTestName.Location = new System.Drawing.Point(12, 73);
            this.tbTestName.Name = "tbTestName";
            this.tbTestName.Size = new System.Drawing.Size(366, 22);
            this.tbTestName.TabIndex = 1;
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOK.Location = new System.Drawing.Point(121, 114);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(152, 29);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "Text";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // nudTestNum
            // 
            this.nudTestNum.Location = new System.Drawing.Point(222, 12);
            this.nudTestNum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudTestNum.Name = "nudTestNum";
            this.nudTestNum.Size = new System.Drawing.Size(69, 22);
            this.nudTestNum.TabIndex = 3;
            // 
            // lblTestNum
            // 
            this.lblTestNum.AutoSize = true;
            this.lblTestNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTestNum.Location = new System.Drawing.Point(12, 11);
            this.lblTestNum.Name = "lblTestNum";
            this.lblTestNum.Size = new System.Drawing.Size(204, 20);
            this.lblTestNum.TabIndex = 4;
            this.lblTestNum.Text = "Введите номер теста: ";
            // 
            // ForTestEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 164);
            this.Controls.Add(this.lblTestNum);
            this.Controls.Add(this.nudTestNum);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tbTestName);
            this.Controls.Add(this.lblinstruction);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ForTestEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Работа с тестами";
            ((System.ComponentModel.ISupportInitialize)(this.nudTestNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblinstruction;
        private System.Windows.Forms.TextBox tbTestName;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.NumericUpDown nudTestNum;
        private System.Windows.Forms.Label lblTestNum;
    }
}