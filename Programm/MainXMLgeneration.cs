﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data.SQLite;
using System.Windows.Forms;

namespace Programm
{
    public class MainXMLgeneration
    {
        public MainXMLgeneration(List<string> section, List<string> topic)
        {
            GetSections(section); //получил список разделов

            List<XElement> content2 = new List<XElement>();
            List<XElement> content = new List<XElement>();

            List<string> used = new List<string>(); //чтобы не повторялись названия тем в дереве
            bool flagUsed = false;
           // string[] split;
            foreach (var item in section) //для каждого раздела
            {             
                GetTopics(item, topic); //получил список тем для раздела
                foreach (var top in topic)
                {
                    flagUsed = false;
                    string[] split = top.Split(';');
                    for (int it=0; it<used.Count; it++) //проверили есть ли уже такой и установили флаг, если есть
                    {
                        if (used[it]==split[0])
                        {
                            flagUsed = true;
                        }
                    }
                    if (flagUsed == false)
                    {
                        content2.Add(new XElement("Вершина",
                            new XAttribute("Имя", split[0])));

                        used.Add(split[0]);
                    }
                        
                }  
                content.Add(new XElement("Вершина",
                    new XAttribute("Имя", item),
                    new XElement("Теория"),
                    new XElement("Лабораторные_работы", content2)));

                topic.Clear();
                content2.Clear();
            }     
        
            var paramArr = content.ToArray();
   
            // create one root element
            var rootElement = new XElement("Дерево",
                new XElement("Вершины",paramArr)); 
            XDocument xDoc = new XDocument(rootElement);
            xDoc.Save("TV.xml");
        }

        public void GetSections(List<string> section)
        {
            using (var dataBase = new MyDatabase())
            {
                string queryGetSection = @" SELECT DISTINCT Name_Work FROM Work";
                SQLiteCommand commandqueryGetSection = new SQLiteCommand(queryGetSection, dataBase.sConnect);

                var reader = commandqueryGetSection.ExecuteReader(); //запомнили все разделы в список
                while (reader.Read())
                {
                    section.Add((string)reader["Name_Work"]);
                }
            }          
        }

        public void GetTopics(string cur_section, List<string> topic)
        {
            using (var dataBase = new MyDatabase())
            {
                //topic = new List<string>();

                string queryGetTopics = @" SELECT Var FROM Work WHERE Name_Work=:name_work";
                SQLiteCommand commandqueryGetTopics = new SQLiteCommand(queryGetTopics, dataBase.sConnect);
                commandqueryGetTopics.Parameters.Add(new SQLiteParameter("name_work", cur_section));

                var reader = commandqueryGetTopics.ExecuteReader(); //запомнили все разделы в список
                while (reader.Read())
                {
                    topic.Add((string)reader["Var"]);
                }

            }
        }
   
    }
}
    
        
   

    

