﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using System.Reflection;
using System.Data.SQLite;
using System.Diagnostics;
using System.Xml.Linq;

// !!! НАЗВАНИЕ документа должно совпадать с названием вершины и с названием столбца в бд Name_Work. 
// !!! Для того, чтобы лаб.работа открылась нужно, чтобы в названии вершины дерева содержалось "Лаб"
namespace Programm
{

    public partial class TV : Form
    {
        public int curUserId; //запоминаем id юзера
        public string Name_Work = ""; //нужен для того, чтобы при переходе между срс варианты не менялись
        Dictionary<string, int> variants = new Dictionary<string, int>();
        Dictionary<string, int> variantsSrs = new Dictionary<string, int>();
        TreeNode curNodeText; // храним кликнутую вершину в главном дереве
        public List<string> section; //список разделов для XML
        public List<string> topic; //список тем для XML
        public TV(string _lastname, string _group, bool _isTeacher, int _curUserId)
        {
            InitializeComponent();
            curUserId = _curUserId;
            TreeNodeMouseClickEventArgs e = null;
            lblGroup.Text = _group;
            lblFIO.Text = _lastname;

            if (_isTeacher == false)
            {
                btnDeleteTest.Visible = false;
                btnAddTest.Visible = false;
                pbReload.Visible = false;
            }
            else             {
                btnCopy.Visible = false;
                btnSendWork.Visible = false;

            }
            //Add(this, "Resources/example.xml", null, ClassesTreeView); //считывание xml
            //Add(this, "Resources/Tests.xml", null, TVtests); //считывание xml

            //XML-генерация!!!
            section = new List<string>();
            topic = new List<string>();
            MainXMLgeneration mainXMLgeneration = new MainXMLgeneration(section, topic);
            Add(this, "TV.xml", null, ClassesTreeView); //считывание xml
        }

        public void Add(object sender, String fileName, EventArgs e, TreeView window)
        {
            window.Nodes.Clear(); //почистили treeview чтобы открыть другой
            XmlTextReader reader = new XmlTextReader(fileName);
  
            try
            {
                window.BeginUpdate();
                TreeNode parentNode = null;

                while (reader.Read()) //пока идет считывание
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "Вершина" || reader.Name == "Теория" || reader.Name == "Лабораторные_работы")
                        {
                            TreeNode newNode = new TreeNode();
                            bool isEmptyElement = reader.IsEmptyElement;
                            int attributeCount = reader.AttributeCount;
                            if (attributeCount > 0)
                            {
                                for (int i = 0; i < attributeCount; i++)
                                {
                                    if (i == 0)
                                    {
                                        reader.MoveToAttribute(i);
                                        newNode.Text = reader.Value;
                                    }
                                    else
                                    {
                                        reader.MoveToAttribute(i);
                                        newNode.Tag = reader.Value;
                                    }

                                }
                            }
                            //МАРК ПИСАЛ САМ
                            else
                            {
                                newNode.Text = reader.Name;
                            }
                            // add new node to Parent Node or TreeView
                            if (parentNode != null) parentNode.Nodes.Add(newNode);
                            else window.Nodes.Add(newNode);

                            // making current node 'ParentNode' if its not empty
                            if (!isEmptyElement)
                                parentNode = newNode;
                        }
                    }
                    else if (reader.NodeType == XmlNodeType.EndElement)
                    {
                        if (reader.Name == "Вершина" || reader.Name == "Теория" || reader.Name == "Лабораторные_работы")
                        {
                            parentNode = parentNode.Parent;
                        }
                    }

                    else if (reader.NodeType == XmlNodeType.XmlDeclaration)
                    {
                        //Ignore Xml Declaration                    
                    }
                    else if (reader.NodeType == XmlNodeType.None)
                    {
                        return;
                    }
                    else if (reader.NodeType == XmlNodeType.Text)
                    {
                        parentNode.Nodes.Add(reader.Value);
                    }
                    // moving up to in TreeView if end tag is encountered
                }
            }

            finally
            {
                // enabling redrawing of treeview after all nodes are added
                window.EndUpdate();
                reader.Close();
            }
        }

        public string FullPath(string vertex_name)
        {
            string path = @"Resources\теория и лабораторные\" + vertex_name;
            string absolutePath = Path.GetFullPath((string)path);
            axAcroPDF.src = absolutePath;
            return axAcroPDF.src;
        }

        //PDF
        public void ClassesTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e) // открытие PDF по нажатию на узел
        {
            // придерживаться строгого формата в наименовании файлов : наименование раздела + пробел + наименование листа. типа :Классы. Занятие 1. Лабораторная 1
            
            string  vertex_name = "";
            var listtree = e.Node;
            var node = e.Node;
            while (node.Parent != null)
            {
                node = node.Parent;
            }
            if (e.Node.Level == 0)
            {
                vertex_name = listtree.Text + ".pdf";
            }
            else
            {
                vertex_name = node.Text + ". " + listtree.Text + ".pdf";
            }
          
            FullPath(vertex_name);
           // MessageBox.Show(vertex_name);
            
            GetData(e,node);
        }


        public string GetData(TreeNodeMouseClickEventArgs e, TreeNode node) 
        {
            using (var dataBase = new MyDatabase())
            {
                string srs;
                try
                {
                    srs = e.Node.Text; // запоминаем узел, на который щелкнули
                    curNodeText = e.Node;
                    if (srs.IndexOf("Лабораторная") != -1) //Если есть  в названии узла есть "Лабораторная", то обращаемся к БД 
                    {
                        string srsforsql = srs + '%';
                        // считаем количество вариантов, которые относятся в выбранной лабе
                        string queryCount = @"select count(var) from Work 
                                                where Name_Work = @Name_Work_list  and var like @Name_lab and id_lab is not null";
                        SQLiteCommand commandCount = new SQLiteCommand(queryCount, dataBase.sConnect);
                        commandCount.Parameters.Add(new SQLiteParameter("@Name_Work_list", node.Text));
                        commandCount.Parameters.Add(new SQLiteParameter("@Name_lab", srsforsql));
                        int Count = Convert.ToInt32(commandCount.ExecuteScalar());
                        if (Count != 0)
                        {
                            int variant;
                            if (!variants.TryGetValue(srs, out variant))
                            {
                                Random rand = new Random();
                                variant = rand.Next(1, Count + 1); //выбираем случайный варинт для данной срс
                                                                   // Считываем текст случайного варианта, выбранной срс на экран
                                variants.Add(srs, variant);
                            }
                            //Считываем текст работы с выбранным названием и случайным вариантом
                            //string query = @"select text_srs FROM Work WHERE Name_Work = @Name_Work_list and var=@number_var";

                            string query = @"select Text_Work.Text from Work join Text_Work ON Text_Work.id=Work.id_Lab
                                            where Name_Work= @Name_Work_list and var like @number_var";
                            SQLiteCommand command = new SQLiteCommand(query, dataBase.sConnect);
                            command.Parameters.Add(new SQLiteParameter("@Name_Work_list", node.Text));
                            command.Parameters.Add(new SQLiteParameter("@number_var", srs + ";%" + variant));
                            SQLiteDataReader reader = command.ExecuteReader();

                            //Выводим текст случайного варианта, выбранной срс на экран

                            //if (axAcroPDFLab.Visible == false)
                            //{
                               // axAcroPDFLab.Dispose();
                                Name_Work = srs;
                                var name_file = "";
                                while (reader.Read())
                                {
                                    name_file += reader["Text"];
                                }
                               // MessageBox.Show(name_file);
                                string path = @"Resources\файлы для лабораторных\" + name_file + ".pdf";
                               // MessageBox.Show(path);
                                string absolutePath = Path.GetFullPath((string)path);
                               // MessageBox.Show(absolutePath);
                                axAcroPDFLab.src = absolutePath;
                            //}
                            //Пересчитываем текст, только если название лабы сменилось.
                            //if (Name_Work != srs)
                            //{
                            //    Name_Work = srs;
                            //    var name_file = "";
                            //    while (reader.Read())
                            //    {
                            //        name_file += reader["Text"] + "\n";
                            //    }
                            //    string path = @"Resources\файлы для лабораторных\" + name_file + ".pdf";
                            //    string absolutePath = Path.GetFullPath((string)path);
                            //    axAcroPDFLab.src = absolutePath;
                            //}
                            if (File.Exists(absolutePath) == true)
                            {
                                return axAcroPDFLab.src;
                            }
                            else
                            {
                                MessageBox.Show("Варианты к лабораторной ещё не созданы, либо расположение файлов указано неверно");
                                return null;
                            }
                        }
                    }
                    return axAcroPDFLab.src;
                }
                catch
                {
                    MessageBox.Show("Кажется что-то пошло не так...");
                    return "";
                }
            }
        }

        private void LoadSolution_Click(object sender, EventArgs e) 
        {
            MessageBox.Show("Coming soon", "Oooops", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }
     
        private void OpenIDE_Click(object sender, EventArgs e) //кнопка открыть VS
        {
           // System.Diagnostics.Process.Start(@"D:\Microsoft Visual Studio 12.0\Common7\IDE\devenv.exe");
            //System.Diagnostics.Process.Start(@"c:\Program Files\Microsoft Visual Studio 10.0\Common7\IDE\VCSExpress.exe");// мой пк
            ///аудиторный
            Process.Start (@"C:\Program Files\Microsoft Visual Studio 10.0\Common7\IDE\vcsexpress.exe"); 
        }

        private void tabs_Selecting(object sender, TabControlCancelEventArgs e)
        {
            int temp_tab = tabs.SelectedIndex;
           
            if (tabs.SelectedIndex == 2) //тест
            {
                if (Properties.Settings.Default.IsTeacher == false) //студент
                {
                    //уведомление о переходе
                    const string message = "Вы уверены, что хотете перейти к тесту? Другие окна будут недоступны";
                    const string caption = "Подтверждение";
                    var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.No) e.Cancel = true;
                    else
                    {
                        tabs.TabPages[0].Parent = null; //закрыли теорию
                        tabs.TabPages[0].Parent = null; //закрыли лабу  
                    }
                }
                loadTestTree();
            } 
        }

        private void loadTestTree()
        {
            TVtests.Nodes.Clear();

            using (var dataBase = new MyDatabase()) //Заоленение дерева тестов из БД
            {
                string queryCount = @"select Name_Test from Tests";
                SQLiteCommand command = new SQLiteCommand(queryCount, dataBase.sConnect);
                SQLiteDataReader reader = command.ExecuteReader();

                TreeNode Tests = new TreeNode();              

                while (reader.Read())
                {
                    string testsql = (string)reader["Name_Test"];

                    if (testsql != Tests.Text)
                    {
                        Tests = TVtests.Nodes.Add(testsql);
                    }
                }
            }
        }

        private void BtnCopy_Click(object sender, EventArgs e)
        {
            bool anyCopied = false; //флаг на то, скопировалось что-то или нет. нужен для msgbox ниже
            bool anyCopiedSrs = false;
            string pathCopyIN = @"C:\CopiedFiles\" + lblFIO.Text;
            string pathCopyFROM = @"Resources\теория и лабораторные\";
            string absolutePathFRrom = Path.GetFullPath((string)pathCopyFROM);
            string pathCopyFromSrs = @"Resources\файлы для срс";
            string absolutePathFRromSrs = Path.GetFullPath(pathCopyFromSrs);

            //удаляем директорию, если есть (чтобы файлы не накапливались)
            if (Directory.Exists(pathCopyIN))
            {
                Directory.Delete(pathCopyIN, true);
            }

            //копирование
            try
            {
                if (!Directory.Exists(pathCopyIN))
                {
                    DirectoryInfo di = Directory.CreateDirectory(pathCopyIN);
                }
                string[] files = Directory.GetFiles(absolutePathFRrom);
                string[] filesSrs = Directory.GetFiles(absolutePathFRromSrs);

                var listtree = curNodeText;
                if (listtree == null)
                {
                    MessageBox.Show("Выберите занятие");
                }
                else
                {
                    var node = curNodeText;
                    while (node.Parent != null)
                    {
                        node = node.Parent;
                    }

                    foreach (string s in files)
                    {
                        string fileName = Path.GetFileName(s);
                        string[] m = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);//Делим имя по частям, чтобы не задавать его статически
                        string lessonName = m[0] + "." + m[1];
                        if (lessonName == node.Text)
                        {
                            string destFile = Path.Combine(pathCopyIN, fileName);
                            File.Copy(s, destFile, true);
                            anyCopied = true;
                        }
                    }

                    using (var dataBase = new MyDatabase())
                    {
                        var srs = curNodeText.Text; // текст текущей вершины
                        var lastname = "";
                        string queryName = @"select var from Work 
                                                   where  var like 'Лабораторная _;%' and id_srs is not null";
                        SQLiteCommand commandName = new SQLiteCommand(queryName, dataBase.sConnect);
                        SQLiteDataReader reader = commandName.ExecuteReader();
                        var name_file = "";
                        int j = 0;
                        while (reader.Read())
                        {
                            name_file = reader["Var"].ToString();

                            string[] m = name_file.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);//Делим имя по частям, чтобы не задавать его статически
                            //j++;
                            if (lastname != m[0])
                            {
                                lastname = m[0];
                                string queryCount = @"select count(var) from Work 
                                                   where  var like @Name_Work_list and id_srs is not null";
                                SQLiteCommand commandCount = new SQLiteCommand(queryCount, dataBase.sConnect);
                                commandCount.Parameters.Add(new SQLiteParameter("@Name_Work_list", m[0] + "%"));
                                int Count = Convert.ToInt32(commandCount.ExecuteScalar());
                                int variant = 0;
                                if (Count != 0)
                                {
                                    if (!variantsSrs.TryGetValue(name_file, out variant))
                                    {
                                        Random rand = new Random();
                                        variant = rand.Next(1, Count + 1); //выбираем случайный варинт для данной срс                                    
                                        variantsSrs.Add(name_file, variant);//добавляем в Dictionary
                                    }
                                }
                                name_file = name_file.Replace("Лабораторная", "Срс");
                                foreach (string s in filesSrs)
                                {
                                    string fileName = Path.GetFileName(s);

                                    string rename = name_file;
                                    string[] ren = rename.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                                    var name_file_srs = ren[0];

                                    var fileShablon = node.Text + ". " + name_file_srs + ";вариант " + variant + ".pdf";
                                    if (fileName == fileShablon && variant != 0)
                                    {
                                        string destFileSrs = Path.Combine(pathCopyIN, fileName);
                                        File.Copy(s, destFileSrs, true);
                                        anyCopiedSrs = true;
                                    }
                                }
                                name_file = "";
                            }
                        }

                        if (anyCopied) MessageBox.Show("Файлы по теме " + node.Text + " успешно скопированы в директорию: " + pathCopyIN);
                        else MessageBox.Show("Кликните на нужное занятие в дереве");
                        anyCopiedSrs = false;
                        anyCopied = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TVtests_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string vertex_name = e.Node.Text;
            string path = @"Resources\";
            string absolutePath = Path.GetFullPath(path);
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = absolutePath + vertex_name + ".exe",
                    RedirectStandardInput = true,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                }
            };
            process.Start();

            process.WaitForExit();
            //считывание результата теста
            if (process.HasExited)
            {
                string fullpath="";

                //берем полный(!) путь до файла (включая сам файл)
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fullpath = openFileDialog.FileName;
                }

                //получаем результат теста
                //!!!!
                //ParseWordTest word_text = new ParseWordTest();
                //string test_result = word_text.ShowWord(fullpath);
                string test_result = "результат";


                string work_name=SaveUserResult.ParseTestNodeName(vertex_name);
                SaveUserResult.FillTestResult(work_name, test_result,curUserId, "тест");
            }
        }

        private void Back(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnSendWork_Click_1(object sender, EventArgs e)
        {
            Visible = false;
            new TestTasks(true, false, curUserId).ShowDialog();
            Visible = true;
        }

        private void btnAddTest_Click(object sender, EventArgs e)
        {
            new ForTestEdit("Добавление").Show();
        }

        private void btnDeleteTest_Click(object sender, EventArgs e)
        {
            ForTestEdit testEdit = new ForTestEdit("Удаление");
            testEdit.Show();
        }

        private void pbReload_Click(object sender, EventArgs e)
        {
            loadTestTree();
        }

    }
}
