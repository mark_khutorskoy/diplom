﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

//!!!Чтобы тесты добавлялись в БД упорядоченно, нужно, чтобы номер теста совпадал с id в БД!

namespace Programm
{
    public partial class ForTestEdit : Form
    {
        string _mode;
        public ForTestEdit(string mode)
        {
            InitializeComponent();
            _mode = mode;

            if (_mode=="Удаление")
            {
                lblinstruction.Visible = false;
                tbTestName.Visible = false;

                btnOK.Text = "Удалить тест";
            }
            else
            {
                btnOK.Text = "Добавить тест";
            }
        }
        
        public void AddTest()
        {
            using (var dataBase = new MyDatabase())
            {
                string queryAddTest = @"INSERT INTO Tests (id,Name_Test) VALUES (@id, @Name_Test)";
                SQLiteCommand commandqueryAddTest = new SQLiteCommand(queryAddTest, dataBase.sConnect);
                commandqueryAddTest.Parameters.Add(new SQLiteParameter("id", nudTestNum.Value));
                commandqueryAddTest.Parameters.Add(new SQLiteParameter("Name_Test", tbTestName.Text));
                commandqueryAddTest.ExecuteNonQuery();
                MessageBox.Show("Тест добавлен");
            }
        }

        //поскольку id в таблице всегда совпдает с номером теста, то можно искать просто по id (а не сплитить название)
        public void DeleteTest()
        {
            using (var dataBase = new MyDatabase())
            {
                string queryDeleteTest = @"DELETE FROM Tests WHERE id=:id";
                SQLiteCommand commandqueryDeleteTest = new SQLiteCommand(queryDeleteTest, dataBase.sConnect);
                commandqueryDeleteTest.Parameters.Add(new SQLiteParameter("id", nudTestNum.Value));
                //commandqueryDeleteTest.Parameters.Add(new SQLiteParameter("Name_Test", tbTestName.Text));
                commandqueryDeleteTest.ExecuteNonQuery();
                MessageBox.Show("Тест удален");
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (_mode == "Удаление") DeleteTest();
            else AddTest();
        }


    }
}
